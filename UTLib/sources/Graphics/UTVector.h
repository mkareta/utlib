/*
 * File:    UTVector.h
 *
 * Created by Maksym Kareta on 1/23/12.
 * Copyright 2012 Maksym Kareta. All rights reserved.
 *
 */

#import <CoreGraphics/CGGeometry.h>
#import "UTDefinitionsC.h"
#import "UTMath.h"

typedef CGPoint UTVector;

extern const UTVector UTVectorZero;

#ifdef UT_DEBUG //For type cheking in debug mode

UT_ALWAYS_INLINE CGPoint UTVectorConvertToPoint(UTVector x)
{
	return (CGPoint)x;
}

UT_ALWAYS_INLINE CGPoint UTVectorFromPoint(CGPoint x)
{
	return (UTVector)x;
}

#else

#define UTVectorConvertToPoint(x) ((CGPoint)x)
#define UTVectorFromPoint(x) ((UTVector)x)

#endif

UT_STATIC_INLINE UTVector UTVectorMake(CGFloat x, CGFloat y)
{
    UTVector v;
    v.x = x;
    v.y = y;
    return v;
}

UT_STATIC_INLINE UTVector UTVectorSubtracVectors(UTVector minuend, UTVector subtrahend)
{
    return UTVectorMake(minuend.x - subtrahend.x, minuend.y - subtrahend.y);
}

UT_STATIC_INLINE UTVector UTVectorMakeWithPoints(CGPoint x, CGPoint y)
{
    return UTVectorSubtracVectors(x, y);
}

UT_STATIC_INLINE UTVector UTVectorRotate(UTVector vector, CGFloat angle)
{
    CGFloat c = UTCos(angle);
    CGFloat s = UTSin(angle);
    return UTVectorMake(c * vector.x - s * vector.y, s * vector.x + c * vector.y);
}

UT_STATIC_INLINE CGFloat UTVectorLength(UTVector vector)
{
    return UTSqrt(vector.x * vector.x + vector.y * vector.y);
}

UT_EXTERN UTVector UTVectorVectorsAverage(UTVector vector1, UTVector vector2);
UT_EXTERN UTVector UTVectorSumVectors(UTVector vector1, UTVector vector2);
UT_EXTERN UTVector UTVectorScale(UTVector vector, CGFloat sx, CGFloat sy);
UT_EXTERN UTVector UTVectorMultipliedByScalar(UTVector vector, CGFloat scalar);
UT_EXTERN UTVector UTVectorNormalize(UTVector vector);
UT_EXTERN UTVector UTVectorSetLength(UTVector vector, CGFloat length);
UT_EXTERN CGFloat UTVectorAngle(UTVector vector);
UT_EXTERN CGFloat UTVectorAngleSin(UTVector vector);
UT_EXTERN CGFloat UTVectorAngleCos(UTVector vector);
UT_EXTERN CGFloat UTVectorWedgeProduct(UTVector vector1, UTVector vector2);
UT_EXTERN CGFloat UTVectorAngleBetweenVectors(UTVector vector1, UTVector vector2);
UT_EXTERN CGFloat UTVectorSinOfAngleBetweenVectors(UTVector vector1, UTVector vector2);
UT_EXTERN CGFloat UTVectorScalarMul(UTVector vec1, UTVector vec2);

/*
 * Result is vector1 + step * vector2
 * See impl for details
 */
UT_EXTERN UTVector UTVectorSmoothStep(UTVector vector1, UTVector vector2, CGFloat step);


UT_EXTERN void UTVectorMultiplyByScalar(UTVector *inOutVector1, CGFloat scalar);
UT_EXTERN void UTVectorAddVector(UTVector *inOutVector1, UTVector vector2);

