//
//  UTMath.m
//  UTLib
//
//  Created by Maksym Kareta on 3/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTMath.h"

float __attribute__((overloadable)) UTCos(const float val)
{
    return cosf(val);
}

double __attribute__((overloadable)) UTCos(const double val)
{
    return cos(val);
}

float __attribute__((overloadable)) UTAcos(const float val)
{
    return acosf(val);
}

double __attribute__((overloadable)) UTAcos(const double val)
{
    return acos(val);
}

float __attribute__((overloadable)) UTSin(const float val)
{
    return sinf(val);
}

double __attribute__((overloadable)) UTSin(const double val)
{
    return sin(val);
}

float __attribute__((overloadable)) UTAsin(const float val)
{
    return asinf(val);
}

double __attribute__((overloadable)) UTAsin(const double val)
{
    return asin(val);
}

float __attribute__((overloadable)) UTSqrt(const float val)
{
    return sqrtf(val);
}

double __attribute__((overloadable)) UTSqrt(const double val)
{
    return sqrt(val);
}

