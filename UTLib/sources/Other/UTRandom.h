//
//  UTRandom.h
//  UTLib
//
//  Created by Maksym Kareta on 6/19/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#ifdef __cplusplus
extern "C" {
#endif

extern NSInteger UTRandomIntegerInRange(NSInteger low, NSInteger high);
extern NSInteger UTRandomIntegerInRadius(NSInteger number, NSInteger radius);
extern CGFloat UTRandomFloatInRange(CGFloat low, CGFloat high);
extern u_int8_t UTRandom8(void);
extern u_int16_t UTRandom16(void);
extern u_int32_t UTRandom32(void);
extern u_int64_t UTRandom64(void);

#ifdef __cplusplus
}
#endif
