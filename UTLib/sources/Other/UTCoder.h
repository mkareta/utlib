//
//  UTCoder.h
//  UTLib
//
//  Created by Max Kareta on 25/03/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UTCoder : NSObject

+ (NSData *)encodeObject:(id)object;
+ (BOOL)writeObject:(id)object toPath:(NSString *)path attributes:(NSDictionary<NSFileAttributeKey,id> *)attributes error:(NSError **)error;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithStream:(NSOutputStream *)stream;
- (instancetype)initWithStream:(NSOutputStream *)stream cache:(NSMutableDictionary *)cache NS_DESIGNATED_INITIALIZER;

- (void)encodeObject:(id)object forKey:(NSString *)key;

- (void)encodeString:(NSString *)string forKey:(NSString *)key;
- (void)encodeArray:(NSArray *)array forKey:(NSString *)key;
- (void)encodeDictionary:(NSDictionary *)array forKey:(NSString *)key;
- (void)encodeData:(NSData *)value forKey:(NSString *)key;

- (void)encodeNumber:(NSNumber *)number forKey:(NSString *)key;
- (void)encodeValue:(NSValue *)value forKey:(NSString *)key;

- (void)encodeDouble:(double)value forKey:(NSString *)key;
- (void)encodeFloat:(float)value forKey:(NSString *)key;
- (void)encodeInt8:(int8_t)value forKey:(NSString *)key;
- (void)encodeInt16:(int16_t)value forKey:(NSString *)key;
- (void)encodeInt32:(int32_t)value forKey:(NSString *)key;
- (void)encodeInt64:(int64_t)value forKey:(NSString *)key;
- (void)encodeInteger:(NSInteger)value forKey:(NSString *)key;
- (void)encodeBool:(BOOL)value forKey:(NSString *)key;

@end

@interface UTDecoder : NSObject

+ (id)decodeData:(NSData *)data;
+ (id)readObjectFromPath:(NSString *)path error:(NSError *__autoreleasing*)error;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithStream:(NSInputStream *)stream;


- (id)decodeObjectForKey:(NSString *)key;

- (NSString *)decodeStringForKey:(NSString *)key;
- (NSArray *)decodeArrayForKey:(NSString *)key;
- (NSDictionary *)decodeDictionaryForKey:(NSString *)key;
- (NSData *)decodeDataForKey:(NSString *)key;

- (NSNumber *)decodeNumberForKey:(NSString *)key;
- (NSValue *)decodeValueForKey:(NSString *)key;

- (double)decodeDoubleForKey:(NSString *)key;
- (float)decodeFloatForKey:(NSString *)key;
- (int8_t)decodeInt8ForKey:(NSString *)key;
- (int16_t)decodeInt16ForKey:(NSString *)key;
- (int32_t)decodeInt32ForKey:(NSString *)key;
- (int64_t)decodeInt64ForKey:(NSString *)key;
- (NSInteger)decodeIntegerForKey:(NSString *)key;
- (BOOL)decodeBoolForKey:(NSString *)key;

@end

@protocol UTCoder

@required

- (instancetype)initWithUTCoder:(UTDecoder *)aDecoder;
- (void)encodeWithUTCoder:(UTCoder *)aCoder;

@optional

+ (NSArray *)keyPathArrayForUTCoder;

@end

