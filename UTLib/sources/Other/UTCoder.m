//
//  UTCoder.m
//  UTLib
//
//  Created by Max Kareta on 25/03/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTCoder.h"

#define UTCoderReadAndValidate(TO, LEN, FLAG) {*FLAG = ([stream read:(uint8_t *)(TO) maxLength:LEN] == LEN);}
#define UTCoderWrite(FROM, LEN) { if (LEN > 0) {[stream write:(const uint8_t *)(FROM) maxLength:LEN];}}
#define UTCoderCheckValid() if (!*valid) {return nil;}
#define UTCoderCheckValidAndBreak() if (!valid) {break;}
#define UTCoderEncodeObjectKVPMethodBody(X, Y) \
- (void)encode##X:(Y)object forKey:(NSString *)key \
{ \
    if (nil != object) \
    { \
        [self writeKVPKey:key];\
        UTCoderWriteObject(self.stream, object, self.cache); \
    } \
}

#define UTCoderEncodeValueKVPMethodBody(X, Y) \
- (void)encode##X:(Y)value forKey:(NSString *)key \
{ \
    [self writeKVPKey:key]; \
    UTCoderWrite##X(self.stream, value); \
}

#define UTCoderDecodeObjectKVPMethodBody(X, Y) \
- (Y)decode##X##ForKey:(NSString *)key \
{ \
    return self.decodedData[key];\
}

#define UTCoderDecodeValueKVPMethodBody(X, Y, Z) \
- (Y)decode##X##ForKey:(NSString *)key \
{ \
    return [self.decodedData[key] Z];\
}

#if __has_feature(objc_arc)
#error this file should be compiled without arc 
#endif

static const int8_t kUTCoderDataTypeBool = 1;
static const int8_t kUTCoderDataTypeInt8 = 2;
static const int8_t kUTCoderDataTypeInt16 = 3;
static const int8_t kUTCoderDataTypeInt32 = 4;
static const int8_t kUTCoderDataTypeInt64 = 5;
static const int8_t kUTCoderDataTypeDouble = 6;
static const int8_t kUTCoderDataTypeFloat = 7;
static const int8_t kUTCoderDataTypeString = 8;
static const int8_t kUTCoderDataTypeArray = 9;
static const int8_t kUTCoderDataTypeDictionary = 10;
static const int8_t kUTCoderDataTypeObject = 11;
static const int8_t kUTCoderDataTypeData = 12;
static const int8_t kUTCoderDataTypeNull = 13;
static const int8_t kUTCoderDataTypeValue = 14;
static const int8_t kUTCoderDataTypeCachedObject = 15;
static const int8_t kUTCoderDataTypeObjectCachedClassName = 16;

static const int8_t kUTCoderKVPFlag = 1;
static const int8_t kUTCoderKVPCacheFlag = 2;
static const int8_t kUTCoderEndFlag = 3;

void UTCoderWriteKVPFlag(NSOutputStream *stream);
void UTCoderWriteEndFlag(NSOutputStream *stream);
void UTCoderWriteKVPCacheFlag(NSOutputStream *stream);

void UTCoderWriteKey(NSOutputStream *stream, NSString *key);

void UTCoderWriteData(NSOutputStream *stream, NSData *newData);
void UTCoderWriteBool(NSOutputStream *stream, int8_t value);
void UTCoderWriteInt8(NSOutputStream *stream, int8_t value);
void UTCoderWriteInt16(NSOutputStream *stream, int16_t value);
void UTCoderWriteInt32(NSOutputStream *stream, int32_t value);
void UTCoderWriteInt32Raw(NSOutputStream *stream, int32_t value);
void UTCoderWriteInt64(NSOutputStream *stream, int64_t value);
void UTCoderWriteInteger(NSOutputStream *stream, NSInteger value);
void UTCoderWriteDouble(NSOutputStream *stream, double value);
void UTCoderWriteFloat(NSOutputStream *stream, float value);
void UTCoderWriteObject(NSOutputStream *stream, id object, NSMutableDictionary *cache);
void UTCoderWriteString(NSOutputStream *stream, NSString *string, NSMutableDictionary *cache);
void UTCoderWriteStringRaw(NSOutputStream *stream, NSString *string);
void UTCoderWriteArray(NSOutputStream *stream, NSArray *array, NSMutableDictionary *cache);
void UTCoderWriteDictionary(NSOutputStream *stream, NSDictionary *dict, NSMutableDictionary *cache);
void UTCoderWriteUknownObject(NSOutputStream *stream, id object, NSMutableDictionary *cache);
void UTCoderWriteCachedObject(NSOutputStream *stream, NSNumber *index);
void UTCoderWriteNull(NSOutputStream *stream);

void UTCoderWriteNumber(NSOutputStream *stream, NSNumber *number);
void UTCoderWriteValue(NSOutputStream *stream, NSValue *value);

int8_t UTCoderGetType(NSInputStream *stream, BOOL *valid);
id UTCoderGetObject(NSInputStream *stream, BOOL *valid, NSMutableArray *cache);
id UTCoderGetUknownObject(NSInputStream *stream, BOOL *valid, int8_t type, NSMutableArray *cache);
BOOL UTCoderGetBool(NSInputStream *stream, BOOL *valid);
int8_t UTCoderGetInt8(NSInputStream *stream, BOOL *valid);
int16_t UTCoderGetInt16(NSInputStream *stream, BOOL *valid);
int32_t UTCoderGetInt32(NSInputStream *stream, BOOL *valid);
int64_t UTCoderGetInt64(NSInputStream *stream, BOOL *valid);
double UTCoderGetDouble(NSInputStream *stream, BOOL *valid);
NSValue *UTCoderGetValue(NSInputStream *stream, BOOL *valid);
float UTCoderGetFloat(NSInputStream *stream, BOOL *valid);
NSString *UTCoderGetString(NSInputStream *stream, BOOL *valid);
NSString *UTCoderGetStringWithLength(NSInputStream *stream, int32_t length, BOOL *valid);
NSArray *UTCoderGetArray(NSInputStream *stream, BOOL *valid, NSMutableArray *cache);
NSDictionary *UTCoderGetDictionary(NSInputStream *strea, BOOL *valid, NSMutableArray *cache);
NSData *UTCoderGetData(NSInputStream *stream, BOOL *valid);
id UTCoderGetCachedObject(NSInputStream *stream, BOOL *valid, NSMutableArray *cache);

@interface UTCoder()

@property (strong, nonatomic) NSOutputStream *stream;
@property (strong, nonatomic) NSMutableDictionary<NSString *, NSNumber *> *cache;

@end

@implementation UTCoder

+ (NSData *)encodeObject:(id)object
{
    NSOutputStream *stream = [NSOutputStream outputStreamToMemory];
    [stream open];
    UTCoder *coder = [[UTCoder alloc] initWithStream:stream];
    [coder encodeObject:object];
    NSData *result = [stream propertyForKey:NSStreamDataWrittenToMemoryStreamKey];
    [stream close];
    [coder release];
    return result;
}

+ (BOOL)writeObject:(id)object toPath:(NSString *)path attributes:(NSDictionary<NSFileAttributeKey,id> *)attributes error:(NSError *__autoreleasing*)error
{
    NSOutputStream *stream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    [stream open];
    if (stream.streamStatus == NSStreamStatusOpen)
    {
        UTCoder *coder = [[UTCoder alloc] initWithStream:stream];
        [coder encodeObject:object];
        [coder release];
        coder = nil;
    }
    if (stream.streamStatus == NSStreamStatusError)
    {
        if (error)
        {
            *error = stream.streamError;
        }
        [stream close];
        return NO;
    }
    [stream close];
    if (attributes)
    {
        return [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:path error:error];
    }
    return YES;
}

- (instancetype)initWithStream:(NSOutputStream *)stream
{
    return [self initWithStream:stream cache:[NSMutableDictionary dictionary]];
}

- (instancetype)initWithStream:(NSOutputStream *)stream cache:(NSMutableDictionary *)cache
{
    self = [super init];
    if (self)
    {
        self.stream = stream;
        self.cache = cache;
    }
    return self;
}

- (void)dealloc
{
    self.stream = nil;
    self.cache = nil;
    [super dealloc];
}

- (void)encodeObject:(id)object
{
    UTCoderWriteObject(self.stream, object, self.cache);
}

- (void)writeKVPKey:(NSString *)key
{
    NSNumber *cache = self.cache[key];
    if (nil != cache)
    {
        UTCoderWriteKVPCacheFlag(self.stream);
        UTCoderWriteInt32Raw(self.stream, [self.cache[key] intValue]);
    }
    else
    {
        self.cache[key] = @([self.cache count]);
        UTCoderWriteKVPFlag(self.stream);
        UTCoderWriteStringRaw(self.stream, key);
    }
}

UTCoderEncodeObjectKVPMethodBody(Array, NSArray *)
UTCoderEncodeObjectKVPMethodBody(Dictionary, NSDictionary *)
UTCoderEncodeObjectKVPMethodBody(Object, id)
UTCoderEncodeObjectKVPMethodBody(String, NSString *)
UTCoderEncodeObjectKVPMethodBody(Data, NSData *)
UTCoderEncodeObjectKVPMethodBody(Number, NSNumber *)
UTCoderEncodeObjectKVPMethodBody(Value, NSValue *)
UTCoderEncodeValueKVPMethodBody(Int8, int8_t)
UTCoderEncodeValueKVPMethodBody(Int16, int16_t)
UTCoderEncodeValueKVPMethodBody(Int32, int32_t)
UTCoderEncodeValueKVPMethodBody(Int64, int64_t)
UTCoderEncodeValueKVPMethodBody(Double, double)
UTCoderEncodeValueKVPMethodBody(Float, float)
UTCoderEncodeValueKVPMethodBody(Bool, BOOL)
UTCoderEncodeValueKVPMethodBody(Integer, NSInteger)

@end

@interface UTDecoder()

@property (strong, nonatomic) NSInputStream *stream;
@property (strong, nonatomic) NSDictionary<NSString *, id> *decodedData;
@property (strong, nonatomic) NSMutableArray *cache;

@end

@implementation UTDecoder

+ (id)decodeData:(NSData *)data
{
    NSInputStream *stream = [NSInputStream inputStreamWithData:data];
    [stream open];
    UTDecoder *decoder = [[UTDecoder alloc] initWithStream:stream];
    id result = [decoder decodeObject];
    [decoder release];
    return result;
}

+ (id)readObjectFromPath:(NSString *)path error:(NSError *__autoreleasing*)error
{
    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:path];
    [stream open];
    if (stream.streamStatus == NSStreamStatusError)
    {
        if (error)
        {
            *error = stream.streamError;
        }
        return nil;
    }
    UTDecoder *decoder = [[UTDecoder alloc] initWithStream:stream];
    id result = [decoder decodeObject];
    [decoder release];
    decoder = nil;
    if (stream.streamStatus == NSStreamStatusError)
    {
        if (error)
        {
            *error = stream.streamError;
        }
        return nil;
    }
    return result;
}

- (instancetype)initWithStream:(NSInputStream *)stream
{
    return [self initWithStream:stream cache:[NSMutableArray array]];
}

- (instancetype)initWithStream:(NSInputStream *)stream cache:(NSMutableArray *)cache
{
    self = [super init];
    if (self)
    {
        self.stream = stream;
        self.cache = cache;
    }
    return self;
}

- (void)dealloc
{
    self.stream = nil;
    self.cache = nil;
    self.decodedData = nil;
    [super dealloc];
}

- (id)decodeObject
{
    BOOL valid = YES;
    //Empty file = nil object
    if (!self.stream.hasBytesAvailable) {
        return nil;
    }
    id object = UTCoderGetObject(self.stream, &valid, self.cache);
    if (!valid)
    {
        return nil;
    }
    return object;
}

- (BOOL)decodeKVPs
{
    BOOL valid = YES;
    NSMutableDictionary *decodedData = [NSMutableDictionary new];
    while (valid)
    {
        int8_t flag = UTCoderGetInt8(self.stream, &valid);
        UTCoderCheckValidAndBreak();
        if (flag == kUTCoderEndFlag)
        {
            break;
        }
        if (flag == kUTCoderKVPFlag || flag == kUTCoderKVPCacheFlag)
        {
            NSString *key = nil;
            if (flag == kUTCoderKVPFlag)
            {
                key = UTCoderGetString(self.stream, &valid);
                if (valid && key)
                {
                    [self.cache addObject:key];
                }
                else
                {
                    valid = NO;
                }
            }
            else if (flag == kUTCoderKVPCacheFlag)
            {
                int32_t index = UTCoderGetInt32(self.stream, &valid);
                key = self.cache[index];
            }
            UTCoderCheckValidAndBreak();
            id object = UTCoderGetObject(self.stream, &valid, self.cache);
            UTCoderCheckValidAndBreak();
            decodedData[key] = object ?: [NSNull null];
            continue;
        }
        valid = NO;
    }
    self.decodedData = decodedData;
    [decodedData release];
    return valid;
}

UTCoderDecodeObjectKVPMethodBody(Object, id)
UTCoderDecodeObjectKVPMethodBody(String, NSString *)
UTCoderDecodeObjectKVPMethodBody(Array, NSArray *)
UTCoderDecodeObjectKVPMethodBody(Dictionary, NSDictionary *)
UTCoderDecodeObjectKVPMethodBody(Data, NSData *)
UTCoderDecodeObjectKVPMethodBody(Number, NSNumber *)
UTCoderDecodeObjectKVPMethodBody(Value, NSValue *)

UTCoderDecodeValueKVPMethodBody(Int8, int8_t, charValue)
UTCoderDecodeValueKVPMethodBody(Int16, int16_t, shortValue)
UTCoderDecodeValueKVPMethodBody(Int32, int32_t, intValue)
UTCoderDecodeValueKVPMethodBody(Int64, int64_t, longLongValue)
UTCoderDecodeValueKVPMethodBody(Double, double, doubleValue)
UTCoderDecodeValueKVPMethodBody(Float, float, floatValue)
UTCoderDecodeValueKVPMethodBody(Bool, BOOL, boolValue)
UTCoderDecodeValueKVPMethodBody(Integer, NSInteger, integerValue)

@end

void UTCoderWriteKVPFlag(NSOutputStream *stream)
{
    UTCoderWrite(&kUTCoderKVPFlag, 1);
}

void UTCoderWriteKVPCacheFlag(NSOutputStream *stream)
{
    UTCoderWrite(&kUTCoderKVPCacheFlag, 1);
}

void UTCoderWriteEndFlag(NSOutputStream *stream)
{
    UTCoderWrite(&kUTCoderEndFlag, 1);
}

void UTCoderWriteKey(NSOutputStream *stream, NSString *key)
{
    NSData *stringData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    const int32_t length = htonl(1 + stringData.length);
    UTCoderWrite(&length, 4);
    UTCoderWrite(stringData.bytes, stringData.length);
}

void UTCoderWriteBool(NSOutputStream *stream, int8_t value)
{
    UTCoderWrite(&kUTCoderDataTypeBool, 1);
    UTCoderWrite(&value, 1);
}

void UTCoderWriteNumber(NSOutputStream *stream, NSNumber *number)
{
    const char *encoding = [number objCType];
    size_t len = strlen(encoding);
    if (len == 1)
    {
        if (encoding[0] == 'q' ||
            encoding[0] == 'Q')
        {
            UTCoderWriteInt64(stream, [number longLongValue]);
            return;
        }
        if (encoding[0] == 'c' ||
            encoding[0] == 'C' ||
            encoding[0] == 'B')
        {
            UTCoderWriteInt8(stream, [number charValue]);
            return;
        }
        if (encoding[0] == 's' ||
            encoding[0] == 'S')
        {
            UTCoderWriteInt16(stream, [number shortValue]);
            return;
        }
        if (encoding[0] == 'i' ||
            encoding[0] == 'l' ||
            encoding[0] == 'I' ||
            encoding[0] == 'L')
        {
            UTCoderWriteInt32(stream, [number intValue]);
            return;
        }
    }
    UTCoderWriteValue(stream, number);
}

void UTCoderWriteValue(NSOutputStream *stream, NSValue *value)
{
    const char *encoding = [value objCType];
    NSUInteger size = 0;
    int32_t encodignLength = (int32_t)strlen(encoding);
    int32_t encodingSize = htonl(encodignLength);
    NSGetSizeAndAlignment(encoding, &size, NULL);
    int32_t sizeToWrite = htonl(size);
    void *pointer = malloc(size);
    [value getValue:pointer size:size];
    
    UTCoderWrite(&kUTCoderDataTypeValue, 1);
    UTCoderWrite(&encodingSize, 4);
    UTCoderWrite(encoding, encodignLength);
    UTCoderWrite(&sizeToWrite, 4);
    UTCoderWrite(pointer, size);
    
    free(pointer);
}

void UTCoderWriteInt8(NSOutputStream *stream, int8_t value)
{
    UTCoderWrite(&kUTCoderDataTypeInt8, 1);
    UTCoderWrite(&value, 1);
}

void UTCoderWriteInt16(NSOutputStream *stream, int16_t value)
{
    int16_t finalValue = htons(value);
    UTCoderWrite(&kUTCoderDataTypeInt16, 1);
    UTCoderWrite(&finalValue, 2);
}

void UTCoderWriteInt32Raw(NSOutputStream *stream, int32_t value)
{
    int32_t finalValue = htonl(value);
    UTCoderWrite(&finalValue, 4);
}

void UTCoderWriteInt32(NSOutputStream *stream, int32_t value)
{
    int32_t finalValue = htonl(value);
    UTCoderWrite(&kUTCoderDataTypeInt32, 1);
    UTCoderWrite(&finalValue, 4);
}

void UTCoderWriteInt64(NSOutputStream *stream, int64_t value)
{
    int64_t finalValue = htonll(value);
    UTCoderWrite(&kUTCoderDataTypeInt64, 1);
    UTCoderWrite(&finalValue, 8);
}

void UTCoderWriteDouble(NSOutputStream *stream, double value)
{
    int64_t finalValue = htonll(*((int64_t *)&value));
    UTCoderWrite(&kUTCoderDataTypeDouble, 1);
    UTCoderWrite(&finalValue, 2);
}

void UTCoderWriteFloat(NSOutputStream *stream, float value)
{
    int32_t finalValue = htonl(*((int32_t *)&value));
    UTCoderWrite(&kUTCoderDataTypeFloat, 1);
    UTCoderWrite(&finalValue, 2);
}

void UTCoderWriteInteger(NSOutputStream *stream, NSInteger value)
{
    UTCoderWriteInt64(stream, (int64_t)value);
}

void UTCoderWriteString(NSOutputStream *stream, NSString *string, NSMutableDictionary *cache)
{
    UTCoderWrite(&kUTCoderDataTypeString, 1);
    UTCoderWriteStringRaw(stream, string);
}

void UTCoderWriteStringRaw(NSOutputStream *stream, NSString *string)
{
    NSData *stringData = [string dataUsingEncoding:NSUTF8StringEncoding];
    int32_t length = htonl(stringData.length);
    UTCoderWrite(&length, 4);
    UTCoderWrite(stringData.bytes, stringData.length);
}

void UTCoderWriteArray(NSOutputStream *stream, NSArray *array, NSMutableDictionary *cache)
{
    UTCoderWrite(&kUTCoderDataTypeArray, 1);
    int32_t count = htonl(array.count);
    UTCoderWrite(&count, 4);
    
    for (id subObject in array)
    {
        UTCoderWriteObject(stream, subObject, cache);
    }
}

void UTCoderWriteDictionary(NSOutputStream *stream, NSDictionary *dict, NSMutableDictionary *cache)
{
    UTCoderWrite(&kUTCoderDataTypeDictionary, 1);

    int32_t count = htonl(dict.count);
    UTCoderWrite(&count, 4);
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * _Nonnull stop) {
        UTCoderWriteObject(stream, key, cache);
        UTCoderWriteObject(stream, obj, cache);
    }];
}

void UTCoderWriteData(NSOutputStream *stream, NSData *newData)
{
    UTCoderWrite(&kUTCoderDataTypeData, 1);
    int32_t dataLength = htonl(newData.length);
    UTCoderWrite(&dataLength, 4);
    UTCoderWrite(newData.bytes, newData.length);
}

void UTCoderWriteNull(NSOutputStream *stream)
{
    UTCoderWrite(&kUTCoderDataTypeNull, 1);
}

void UTCoderWriteCachedObject(NSOutputStream *stream, NSNumber *index)
{
    UTCoderWrite(&kUTCoderDataTypeCachedObject, 1);
    UTCoderWriteInt32Raw(stream, [index intValue]);
}

void UTCoderWriteObject(NSOutputStream *stream, id object, NSMutableDictionary *cache)
{
    if (!object)
    {
        UTCoderWriteNull(stream);
        return;
    }
    
    NSNumber *value = [NSNumber numberWithInteger:(NSInteger)object];
    NSNumber *index = cache[value];
    if (nil != index)
    {
        UTCoderWriteCachedObject(stream, index);
        return;
    }
    cache[value] = @(cache.count);
    if ([object isKindOfClass:[NSNull class]])
    {
        UTCoderWriteNull(stream);
    }
    else if ([object isKindOfClass:[NSString class]])
    {
        index = cache[object];
        [cache removeObjectForKey:value];
        if (nil != index)
        {
            UTCoderWriteCachedObject(stream, index);
        }
        else
        {
            cache[object] = @(cache.count);
            UTCoderWriteString(stream, object, cache);
        }
    }
    else if ([object isKindOfClass:[NSArray class]])
    {
        UTCoderWriteArray(stream, object, cache);
    }
    else if ([object isKindOfClass:[NSDictionary class]])
    {
        UTCoderWriteDictionary(stream, object, cache);
    }
    else if ([object isKindOfClass:[NSData class]])
    {
        UTCoderWriteData(stream, object);
    }
    else if ([object isKindOfClass:[NSNumber class]])
    {
        //caching numbers is useless
        [cache removeObjectForKey:value];
        UTCoderWriteNumber(stream, object);
    }
    else if ([object isKindOfClass:[NSValue class]])
    {
        UTCoderWriteValue(stream, object);
    }
    else
    {
        UTCoderWriteUknownObject(stream, object, cache);
    }
}

void UTCoderWriteUknownObject(NSOutputStream *stream, id object, NSMutableDictionary *cache)
{
    NSString *className = NSStringFromClass([object class]);
    NSNumber *index = cache[className];
    if (nil != index)
    {
        UTCoderWrite(&kUTCoderDataTypeObjectCachedClassName, 1);
        UTCoderWriteInt32Raw(stream, index.intValue);
    }
    else
    {
        cache[className] = @(cache.count);
        UTCoderWrite(&kUTCoderDataTypeObject, 1);
        NSData *stringData = [className dataUsingEncoding:NSUTF8StringEncoding];
        assert(stringData.length < UINT8_MAX);
        uint8_t length = (uint8_t)stringData.length;
        UTCoderWrite(&length, 1);
        UTCoderWrite(stringData.bytes, stringData.length);
    }
    UTCoder *objectCoder = [[UTCoder alloc] initWithStream:stream cache:cache];
    [object encodeWithUTCoder:objectCoder];
    [objectCoder release];
    UTCoderWriteEndFlag(stream);
}

int8_t UTCoderGetType(NSInputStream *stream, BOOL *valid)
{
    int8_t result = -1;
    UTCoderReadAndValidate(&result, 1, valid);
    return result;
}

BOOL UTCoderGetBool(NSInputStream *stream, BOOL *valid)
{
    int8_t result = 0;
    UTCoderReadAndValidate(&result, 1, valid);
    return result == 0 ? YES : NO;
}

int8_t UTCoderGetInt8(NSInputStream *stream, BOOL *valid)
{
    int8_t result = 0;
    UTCoderReadAndValidate(&result, 1, valid);
    return result;
}

int16_t UTCoderGetInt16(NSInputStream *stream, BOOL *valid)
{
    int16_t result = 0;
    UTCoderReadAndValidate(&result, 2, valid);
    return ntohs(result);
}

int32_t UTCoderGetInt32(NSInputStream *stream, BOOL *valid)
{
    int32_t result = 0;
    UTCoderReadAndValidate(&result, 4, valid);
    return ntohl(result);
}

int64_t UTCoderGetInt64(NSInputStream *stream, BOOL *valid)
{
    int64_t result = 0;
    UTCoderReadAndValidate(&result, 8, valid);
    return ntohll(result);
}

double UTCoderGetDouble(NSInputStream *stream, BOOL *valid)
{
    int64_t result = 0;
    UTCoderReadAndValidate(&result, 8, valid);
    result = ntohll(result);
    double finalValue = *((double *)&result);
    return finalValue;
}

float UTCoderGetFloat(NSInputStream *stream, BOOL *valid)
{
    int32_t result = 0;
    UTCoderReadAndValidate(&result, 4, valid);
    result = ntohl(result);
    float finalValue = *((float *)&result);
    return finalValue;
}

NSValue *UTCoderGetValue(NSInputStream *stream, BOOL *valid)
{
    const char *encoding = NULL;
    uint8_t *bytes = NULL;
    NSValue *value = nil;
    
    int32_t encodingSize = UTCoderGetInt32(stream, valid);
    if (*valid)
    {
        encoding = malloc(encodingSize);
        UTCoderReadAndValidate(encoding, encodingSize, valid);
        if (*valid)
        {
            int32_t dataSize = UTCoderGetInt32(stream, valid);
            if (*valid)
            {
                bytes = malloc(dataSize);
                UTCoderReadAndValidate(bytes, dataSize, valid);
                if (*valid)
                {
                    value = [[[NSValue alloc] initWithBytes:bytes objCType:encoding] autorelease];
                }
            }
        }
    }
    if (NULL != bytes)
    {
        free(bytes);
    }
    if (NULL != encoding)
    {
        free((void *)encoding);
    }
    return value;
}

NSString *UTCoderGetString(NSInputStream *stream, BOOL *valid)
{
    int32_t length = 0;
    UTCoderReadAndValidate(&length, 4, valid);length = ntohl(length);
    UTCoderCheckValid();
    return UTCoderGetStringWithLength(stream, length, valid);
}

NSString *UTCoderGetStringWithLength(NSInputStream *stream, int32_t length, BOOL *valid)
{
    if (length == 0)
    {
        return @"";
    }
    void *bytes = malloc(length);
    UTCoderReadAndValidate(bytes, length, valid);
    if (!*valid)
    {
        free(bytes);
        return nil;
    }
    return [[[NSString alloc] initWithBytesNoCopy:bytes length:length encoding:NSUTF8StringEncoding freeWhenDone:YES] autorelease];
}

NSArray *UTCoderGetArray(NSInputStream *stream, BOOL *valid, NSMutableArray *cache)
{
    int32_t count = UTCoderGetInt32(stream, valid);
    UTCoderCheckValid();
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];
    [cache addObject:result];
    for (NSInteger i = 0; i < count; i++)
    {
        id object = UTCoderGetObject(stream, valid, cache);
        UTCoderCheckValid();
        [result addObject:object ?: [NSNull null]];
    }
    return result;
}

NSDictionary *UTCoderGetDictionary(NSInputStream *stream, BOOL *valid, NSMutableArray *cache)
{
    int32_t count = UTCoderGetInt32(stream, valid);
    UTCoderCheckValid();
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:count];
    [cache addObject:result];
    for (NSInteger i = 0; i < count; i++)
    {
        id key = UTCoderGetObject(stream, valid, cache);
        if (!*valid || !key)
        {
            return nil;
        }
        
        id object = UTCoderGetObject(stream, valid, cache);
        UTCoderCheckValid();
        result[key] = object ?: [NSNull null];
    }
    return result;
}

NSData *UTCoderGetData(NSInputStream *stream, BOOL *valid)
{
    int32_t length = UTCoderGetInt32(stream, valid);
    UTCoderCheckValid();
    if (length == 0)
    {
        return [NSData data];
    }
    void *bytes = malloc(length);
    UTCoderReadAndValidate(bytes, length, valid);
    if (!*valid)
    {
        free(bytes);
        return nil;
    }
    return [[[NSData alloc] initWithBytesNoCopy:bytes length:length freeWhenDone:YES] autorelease];
}

id UTCoderGetUknownObject(NSInputStream *stream, BOOL *valid, int8_t type, NSMutableArray *cache)
{
    NSString *className = nil;
    if (type == kUTCoderDataTypeObjectCachedClassName)
    {
        int32_t index = UTCoderGetInt32(stream, valid);
        if (!*valid || index >= cache.count)
        {
            *valid = NO;
            return nil;
        }
        className = cache[index];
    }
    else
    {
        int8_t length = UTCoderGetInt8(stream, valid);
        className = UTCoderGetStringWithLength(stream, (int32_t)length, valid);
        UTCoderCheckValid();
    }
    Class class = NSClassFromString(className);
    if (!class)
    {
        *valid = NO;
        return nil;
    }
    id object = [class alloc];
    [cache addObject:object];
    if (type != kUTCoderDataTypeObjectCachedClassName)
    {
        [cache addObject:className];
    }
    UTDecoder *decoder = [[UTDecoder alloc] initWithStream:stream cache:cache];
    if ([decoder decodeKVPs])
    {
        object = [[object initWithUTCoder:decoder] autorelease];
    }
    else
    {
        [object release];
        object = nil;
        *valid = NO;
    }
    
    [decoder release];
    return object;
}

id UTCoderGetCachedObject(NSInputStream *stream, BOOL *valid, NSMutableArray *cache)
{
    int32_t index = UTCoderGetInt32(stream, valid);
    if (!*valid)
    {
        return nil;
    }
    if (index >= cache.count) {
        *valid = NO;
        return nil;
    }
    return cache[index];
}

id UTCoderGetObject(NSInputStream *stream, BOOL *valid, NSMutableArray *cache)
{
    int8_t type = UTCoderGetType(stream, valid);
    if (type < 0 || !*valid)
    {
        return nil;
    }
    if (type == kUTCoderDataTypeNull)
    {
        return nil;
    }

    if (type == kUTCoderDataTypeCachedObject)
    {
        return UTCoderGetCachedObject(stream, valid, cache);
    }
    id result = nil;
    // Known object
    if (type == kUTCoderDataTypeValue)
    {
        result = UTCoderGetValue(stream, valid);
    }
    if (type == kUTCoderDataTypeString)
    {
        result = UTCoderGetString(stream, valid);
    }
    if (type == kUTCoderDataTypeData)
    {
        result = UTCoderGetData(stream, valid);
    }
    if (result)
    {
        [cache addObject:result];
        return result;
    }
    // Uncached numbers
    if (type == kUTCoderDataTypeBool)
    {
        return [NSNumber numberWithBool:UTCoderGetBool(stream, valid)];
    }
    if (type == kUTCoderDataTypeInt8)
    {
        return [NSNumber numberWithBool:UTCoderGetInt8(stream, valid)];
    }
    if (type == kUTCoderDataTypeInt16)
    {
        return [NSNumber numberWithBool:UTCoderGetInt16(stream, valid)];
    }
    if (type == kUTCoderDataTypeInt32)
    {
        return [NSNumber numberWithInt:UTCoderGetInt32(stream, valid)];
    }
    if (type == kUTCoderDataTypeInt64)
    {
        return [NSNumber numberWithLongLong:UTCoderGetInt64(stream, valid)];
    }
    if (type == kUTCoderDataTypeDouble)
    {
        return [NSNumber numberWithDouble:UTCoderGetDouble(stream, valid)];
    }
    if (type == kUTCoderDataTypeFloat)
    {
        return [NSNumber numberWithFloat:UTCoderGetFloat(stream, valid)];
    }
    // Collections
    if (type == kUTCoderDataTypeArray)
    {
        return UTCoderGetArray(stream, valid, cache);
    }
    if (type == kUTCoderDataTypeDictionary)
    {
        return UTCoderGetDictionary(stream, valid, cache);
    }
   
    // Uknown object
    if (type == kUTCoderDataTypeObject || type == kUTCoderDataTypeObjectCachedClassName)
    {
        return UTCoderGetUknownObject(stream, valid, type, cache);
    }
    *valid = NO;
    return nil;
}


#undef UTCoderReadAndValidate
#undef UTCoderCheckValid
#undef UTCoderWrite
#undef UTCoderEncodeObjectKVPMethodBody
#undef UTCoderEncodeValueKVPMethodBody
#undef UTCoderDecodeObjectKVPMethodBody
#undef UTCoderDecodeValueKVPMethodBody

