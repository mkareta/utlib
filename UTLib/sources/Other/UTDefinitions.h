//
//  UTDefinitions.h
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#ifndef weakify
#define weakify(X) __weak typeof(X) __weak_var_##X = X
#define strongify(X) \
_Pragma("clang diagnostic push")   \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__strong typeof(__weak_var_##X) X = __weak_var_##X \
_Pragma("clang diagnostic pop")
#endif


#define UTCheckOption(OPTIONS, BIT) (((OPTIONS) & (BIT)) > 0)
#define UTCheckOption2(OPTIONS, BIT, BIT2) (((OPTIONS) & (BIT | BIT2)) > 0)
#define UTRemoveOption(OPTIONS, BIT) ((OPTIONS) & (~(BIT)))
#define UTAddOption(X, Y) ((OPTIONS) | (BIT))


#ifdef UT_DEBUG

#define UTAssert(condition, desc, ...) \
do {			\
if (!(condition)) {	\
[[NSAssertionHandler currentHandler] handleFailureInMethod:_cmd \
object:self file:[NSString stringWithUTF8String:__FILE__] \
lineNumber:__LINE__ description:(desc), ##__VA_ARGS__]; \
}			\
} while(0)

#define UT_DEBUG_BEGIN do {
#define UT_DEBUG_END } while(0)

#define UTLOG( s, ... ) NSLog( @"<%p %@:(%d) %s> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

#else

#define UTAssert(condition, desc, ...)
#define UT_DEBUG_BEGIN while (0){
#define UT_DEBUG_END }
#define UTLOG( s, ... )

#endif

