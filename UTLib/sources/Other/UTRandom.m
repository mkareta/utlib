//
//  UTRandom.m
//  UTLib
//
//  Created by Maksym Kareta on 6/19/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTRandom.h"

NSInteger UTRandomIntegerInRange(NSInteger low, NSInteger high)
{
//	NSUInteger diff = (NSUInteger)(high - low);
//	NSInteger rand = (NSInteger)(arc4random() % diff);
//	return low + rand;
// same code
//NOTE: result of arc4random is in range 0 -- 2^32-1, this why used 2 type casting
	return low + (NSInteger)(arc4random() % (NSUInteger)(high - low));	
}

NSInteger UTRandomIntegerInRadius(NSInteger number, NSInteger radius)
{
	return number - radius + (NSInteger)(arc4random() % (NSUInteger)(2 * radius));
}

CGFloat UTRandomFloatInRange(CGFloat low, CGFloat high)
{
//	CGFloat k = 1.0f * (arc4random() / (NSUIntegerMax));
//	return low + k * (high - low);
	return low + (1.0f * arc4random() / (NSUIntegerMax)) * (high - low);
}

u_int8_t UTRandom8(void)
{
    u_int8_t rand = (u_int8_t)arc4random_uniform(UINT8_MAX);
    return rand;
}

u_int16_t UTRandom16(void)
{
    u_int16_t rand = (u_int16_t)arc4random_uniform(UINT16_MAX);
    return rand;
}

u_int32_t UTRandom32(void)
{
    return arc4random();
}

u_int64_t UTRandom64(void)
{
    u_int32_t first = arc4random();
    u_int32_t second = arc4random();
    u_int64_t result = first;
    result = result << 32;
    result = result + second;
    return result;
}
