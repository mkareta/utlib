//
//  UTAnimationSequence.m
//  UTLib
//
//  Created by Maksym Kareta on 3/4/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "UTAnimationSequence.h"
#import "GCD+UTFunctions.h"

@interface UTAnimationSequence()

@property (strong, nonatomic) NSMutableArray *animations;
@property (copy, nonatomic) UIViewUTAnimationsSequenceCompletionBlock completionBlock;

@end

@implementation UTAnimationSequence

- (instancetype)init
{
    self = [super init];
    if (nil != self)
    {
        self.animations = [NSMutableArray array];
    }
    return self;
}

- (void)addAnimationWithDuration:(NSTimeInterval)duration block:(UIViewUTAnimationsSequenceBlock)block
{
    [self addAnimationWithDuration:duration block:block completion:^{}];
}

- (void)addAnimationWithDuration:(NSTimeInterval)duration block:(UIViewUTAnimationsSequenceBlock)block completion:(UIViewUTAnimationsSequenceCompletionBlock)completion
{
    UIViewUTAnimationsSequenceBlock newBlock = [block copy];
    UIViewUTAnimationsSequenceCompletionBlock newBlock2 = [completion copy];
    NSDictionary *dict = @{@"block": newBlock, @"duration": @(duration), @"completion": newBlock2};
    [self.animations addObject:dict];
}

- (void)commitAnimationSequenceWithCompletionBlock:(UIViewUTAnimationsSequenceCompletionBlock)block
{
    self.completionBlock = block;
    [self continueAnimations];

}

- (void)commitAnimationSequence
{
    [self continueAnimations];
}

- (void)continueAnimations
{
    NSMutableArray *array = self.animations;
    if (0 == [array count])
    {
        [self finishAnimations];
    }
    else
    {
        NSDictionary *dict = array[0];
        NSTimeInterval duration = [dict[@"duration"] doubleValue];
        UIViewUTAnimationsSequenceBlock block = dict[@"block"];
        UIViewUTAnimationsSequenceCompletionBlock completion = dict[@"completion"];
        [UIView animateWithDuration:duration animations:block completion:^(BOOL finished) {
            completion();
            [array removeObjectAtIndex:0];
            [self continueAnimations];
        }];
    }
}

- (void)finishAnimations
{
    UTInvokeBlock(self.completionBlock);
    self.completionBlock = nil;
    [self.animations removeAllObjects];
}

@end
