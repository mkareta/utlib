//
//  UTSwitch.h
//  UTLib
//
//  Created by Maksym Kareta on 12/2/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kUTSwitchTypeOval,
    kUTSwitchTypeRectangle,
    kUTSwitchTypePlain
} UTSwitchType;

/*
     For now this class tested only following use cases:
     setThumbImage:[UIImage imageNamed:@"thumb"]
     setThumbTintColor:nil
     setOffTintBorderColor:nil
     setOnTintBorderColor:nil
     setOnTintColor:nil
     setOffTintColor:nil
     setThumbInset:-3.0
     setBackgroundImage:[UIImage imageNamed:@"background"]
 
     setOnTintColor:someColor1
     setThumbTintColor:someColor2
     setOffTintBorderColor:someColor3
     setOnTintBorderColor:someColor3
     setOnTintColor:nil
     setOffTintColor:nil
     setThumbInset:3.0
     setType:kUTSwitchTypeOval
     setShouldAnimateTransform:NO
     setBackgroundImage:nil
     setOnStateInset:2
     setOffStateInset:2
 */


@interface UTSwitch : UIControl <UIGestureRecognizerDelegate>

@property (assign, nonatomic) UTSwitchType type;

@property (assign, nonatomic, getter = isOn) BOOL on;
@property (assign, nonatomic) BOOL shadow;

@property (assign, nonatomic) BOOL shouldAnimateTransform;
@property (assign, nonatomic) CGFloat thumbInset;
@property (assign, nonatomic) CGFloat onStateInset;
@property (assign, nonatomic) CGFloat offStateInset;

@property (strong, nonatomic) IBInspectable UIColor *onTintColor;
@property (strong, nonatomic) IBInspectable UIColor *offTintColor;
@property (strong, nonatomic) IBInspectable UIColor *thumbTintColor;
@property (strong, nonatomic) IBInspectable UIColor *offTintBorderColor;
@property (strong, nonatomic) IBInspectable UIColor *onTintBorderColor;

- (void)setThumbImage:(UIImage *)thumbImage;
- (void)setOnImage:(UIImage *)onImage;
- (void)setOffImage:(UIImage *)offImage;
- (void)setBackgroundImage:(UIImage *)image;
- (void)setOn:(BOOL)on animated:(BOOL)animated;

@end
