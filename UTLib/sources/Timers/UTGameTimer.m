//
//  UTTimer.m
//  UTLib
//
//  Created by Oleg Zinko on 6/18/12.
//  Copyright (c) 2012 Oleg Zinko. All rights reserved.
//

#import "UTGameTimer.h"

static const NSTimeInterval kUTTimerRenewInterval = 10.0f;

@interface UTGameTimer() 

@property (strong, nonatomic, readwrite) NSDate *beginTime;
@property (strong, nonatomic) NSTimer *renewTimer;

@property (assign, nonatomic, readwrite, getter = isPaused) BOOL paused;

@property (assign, nonatomic) NSTimeInterval lastTime;
@property (assign, nonatomic) NSTimeInterval allTime;

@property (assign, nonatomic, getter = isValid) BOOL valid;

@end

@implementation UTGameTimer

@synthesize beginTime = _beginTime;

- (id)init
{
	self = [super init];
	if (nil != self)
	{
        _paused = YES;
        _valid = NO;
	}
	return self;
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [self pauseTimer];
}

- (void)startTimer
{
    NSAssert(self.renewTimer == nil, @"can't start timer twice");
    if (!self.isValid)
    {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        UIApplication *app = [UIApplication sharedApplication];
        [center addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:app];
     
        self.allTime = 0.0f;
        self.valid = YES;
    }
    self.beginTime = [NSDate date];
    self.paused = NO;
    self.renewTimer = [NSTimer scheduledTimerWithTimeInterval:kUTTimerRenewInterval target:self selector:@selector(renewTime:) userInfo:nil repeats:YES];
}

- (void)stopTimer
{
   	[[NSNotificationCenter defaultCenter] removeObserver:self];
    
	self.beginTime = nil;
	self.paused = NO;
	self.allTime = 0.0f;
	self.valid = NO;
	[self.renewTimer invalidate];
    self.renewTimer = nil;
}

- (void)pauseTimer
{
    NSAssert(self.valid && !self.paused, @"Try pause not started or already paused timer");
    self.allTime += [[NSDate date] timeIntervalSinceDate:self.beginTime];
    self.paused = YES;
    [self.renewTimer invalidate];
    self.renewTimer = nil;
}

- (void)renewTime:(id)sender
{
    self.allTime = self.passedTime;
    self.beginTime = [NSDate date];
    self.lastTime = self.allTime;
}

- (NSTimeInterval)passedTime
{
	NSTimeInterval timePassed = self.allTime;
	if (!self.isPaused)
	{
		timePassed += [[NSDate date] timeIntervalSinceDate:self.beginTime];
	}
	return timePassed;
}

@end
