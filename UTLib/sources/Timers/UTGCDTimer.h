//
//  UTGCDTimer.h
//  UTLib
//
//  Created by Maksym Kareta on 10/24/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

typedef void (^UTFireBlock)(BOOL *stop);

@interface UTGCDTimer : NSObject

@property (assign, nonatomic, readonly) NSTimeInterval timeInterval;

///Creates and schudule gcd timer source
///@param seconds Fire time interval in seconds from now
///@param repeats If YES, the timer will repeatedly reschedule itself until invalidated. If NO, the timer will be invalidated after it fire
///@param leewaySeconds The amount of time, in seconds, that the system can defer the timer.
///@param block The block that will be called when timer fire. Can't be NULL
///@return New schuduled UTGCDTimer instance.
+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats leewayTimeInterval:(NSTimeInterval)leewaySeconds fireBlock:(UTFireBlock)block;

///Creates and schudule gcd timer source
///@param interval Fire time interval in nanoseconds from now
///@param repeats If YES, the timer will repeatedly reschedule itself until invalidated. If NO, the timer will be invalidated after it fire
///@param leeway The amount of time, in nanoseconds, that the system can defer the timer.
///@param block The block that will be called when timer fire. Can't be NULL
///@return New schuduled UTGCDTimer instance.
+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withInterval:(uint64_t)interval repeats:(BOOL)repeats leeway:(uint64_t)leeway fireBlock:(UTFireBlock)block;

///Creates and schudule gcd timer source
///@param seconds Fire time interval in seconds from now
///@param repeats If YES, the timer will repeatedly reschedule itself until invalidated. If NO, the timer will be invalidated after it fire
///@param block The block that will be called when timer fire. Can't be NULL
///@note leeway will set to 0
///@return New schuduled UTGCDTimer instance.
+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats fireBlock:(UTFireBlock)block;

///Invalidates timer
///@warning Timer can't be reused after invalidation
- (void)invalidate;

///Reschedule current timer without fire.
- (void)reschedule;

///Reschedule current timer with new timerinverval.
- (void)rescheduleWithInterval:(NSTimeInterval)newInteval;

@end
