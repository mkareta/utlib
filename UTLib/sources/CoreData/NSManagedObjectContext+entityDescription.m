//
//  NSManagedObjectContext+entityDescription.m
//  UTLib
//
//  Created by Maksym Kareta on 4/4/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSManagedObjectContext+entityDescription.h"

@implementation NSManagedObjectContext (entityDescription)

- (NSEntityDescription *)entityForName:(NSString *)string
{
	return [NSEntityDescription entityForName:string inManagedObjectContext:self];
}

@end
