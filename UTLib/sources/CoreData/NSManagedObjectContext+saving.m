//
//  NSManagedObjectContext+printSaveError.m
//  FRS
//
//  Created by Maksym Kareta on 1/23/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSManagedObjectContext+saving.h"

@implementation NSManagedObjectContext (saving)

- (BOOL)saveAndPrintError
{
	NSError *error = nil;
	BOOL result = [self save:&error];
	if (nil != error)
	{
		NSLog(@"saving context error: %@", error);
	}
	return result;
}

- (NSString *)saveAndReturnErrorDescription
{
	NSError *error = nil;
	BOOL result = [self save:&error];
	return result ? nil : [error description];
}

@end
