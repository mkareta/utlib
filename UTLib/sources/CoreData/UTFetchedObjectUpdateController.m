//
//  UTFetchedObjectUpdateController.m
//  UTLib
//
//  Created by Max Kareta on 12/2/15.
//  Copyright © 2015 Max Kareta. All rights reserved.
//

#import "UTFetchedObjectUpdateController.h"
@import CoreData;

@interface UTFetchedObjectUpdateController() {
    BOOL _reportNew;
    BOOL _reportOld;
    BOOL _useSnapshot;
}

@property (strong, nonatomic) NSManagedObject *object;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSMutableDictionary *snapshot;

@end

@implementation UTFetchedObjectUpdateController

- (instancetype)init NS_UNAVAILABLE
{
    return nil;
}

- (instancetype)initWithObject:(NSManagedObject *)object attributes:(NSArray *)attributes options:(UTFetchedObjectUpdateControllerOptions)options
{
    NSParameterAssert(nil != object.managedObjectContext);
    self = [super init];
    if (nil != self)
    {
        _reportNew = (options & kUTFetchedObjectUpdateControllerReportNew) > 0;
        _reportOld = (options & kUTFetchedObjectUpdateControllerReportOld) > 0;
        _useSnapshot = (options & kUTFetchedObjectUpdateControllerUseObjectSnapshot) > 0;
        self.object = object;
        self.context = object.managedObjectContext;
        if (attributes.count == 0)
        {
            attributes = [[[object entity] attributesByName] allKeys];
        }
        self.snapshot = [NSMutableDictionary dictionaryWithCapacity:attributes.count];
        for (NSString *attribute in attributes)
        {
            self.snapshot[attribute] = [object valueForKey:attribute];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextDidChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.context];
    }
    return self;

}

- (instancetype)initWithObject:(NSManagedObject *)object attributes:(NSArray *)attributes
{
    UTFetchedObjectUpdateControllerOptions options = 0;
    options |= kUTFetchedObjectUpdateControllerReportNew;
    options |= kUTFetchedObjectUpdateControllerReportOld;
    options |= kUTFetchedObjectUpdateControllerUseObjectSnapshot;
    return [self initWithObject:object attributes:attributes options:options];
}

- (instancetype)initWithObjectID:(NSManagedObjectID *)objectID attributes:(NSArray *)attributes context:(NSManagedObjectContext *)context
{
    NSArray *persistentStores = context.persistentStoreCoordinator.persistentStores;
    if (![persistentStores containsObject:objectID.persistentStore])
    {
        objectID = [context.persistentStoreCoordinator managedObjectIDForURIRepresentation:objectID.URIRepresentation];
    }
    NSManagedObject *object = [context existingObjectWithID:objectID error:NULL];
    return [self initWithObject:object attributes:attributes];
}

- (instancetype)initWithObjectURI:(NSURL *)URI attributes:(NSArray *)attributes context:(NSManagedObjectContext *)context
{
    NSManagedObjectID *objectID = [context.persistentStoreCoordinator managedObjectIDForURIRepresentation:URI];
    NSManagedObject *object = [context existingObjectWithID:objectID error:NULL];
    return [self initWithObject:object attributes:attributes];
}

- (void)contextDidChange:(NSNotification *)notification
{
    id <UTFetchedObjectUpdateControllerDelegate> delegate = self.delegate;
    if (self.object.isDeleted || self.object.managedObjectContext == nil)
    {
        [delegate objectUpdateControllerObjectWasDeleted:self];
    }
    else
    {
        for (NSManagedObject *object in notification.userInfo[NSUpdatedObjectsKey])
        {
            if (object == self.object)
            {
                if (_useSnapshot)
                {
                    NSDictionary *oldState = [self.snapshot copy];
                    NSMutableDictionary *oldValues = _reportOld ? [NSMutableDictionary dictionary] : nil;
                    NSMutableDictionary *newValues = _reportNew ? [NSMutableDictionary dictionary] : nil;
                    for (NSString *key in oldState)
                    {
                        id value = [self.object valueForKey:key];
                        id oldValue = oldState[key];
                        if (![value isEqualToString:oldValue])
                        {
                            oldValues[key] = oldValue;
                            newValues[key] = value;
                        }
                    }
                    [delegate objectUpdateControllerObjectWasUpdated:self oldValues:oldState newValues:newValues];
                }
                else
                {
                    [delegate objectUpdateControllerObjectWasUpdated:self oldValues:nil newValues:nil];
                }
                break;
            }
        }
    }
}

@end
