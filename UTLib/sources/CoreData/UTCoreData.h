//
//  UTCoreData.h
//  UTLib
//
//  Created by Oleg Zinko on 7/6/12.
//  Copyright (c) 2012 Oleg Zinko. All rights reserved.
//

#ifndef UTLib_UTCoreData_h
#define UTLib_UTCoreData_h

#import "UTStorageProvider.h"
#import "NSManagedObjectContext+saving.h"
#import "NSManagedObjectContext+entityDescription.h"
#import "NSManagedObjectContext+UTMethods.h"
#import "NSManagedObject+UTMethods.h"

#endif
