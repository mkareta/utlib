//
//  UTFetchedObjectsCountController.h
//  UTLib
//
//  Created by Max Kareta on 2/13/16.
//  Copyright © 2016 Max Kareta. All rights reserved.
//

@class NSFetchRequest;

@protocol UTFetchedObjectsCountControllerDelegate;

typedef NS_ENUM(NSInteger, UTFetchedObjectsCountControllerMode)
{
    kUTFetchedObjectsCountControllerUpdateOnChange = 0,
    kUTFetchedObjectsCountControllerUpdateOnContextSave = 1
};

@interface UTFetchedObjectsCountController : NSObject

@property (assign, nonatomic) id <UTFetchedObjectsCountControllerDelegate> delegate;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFetchRequest:(NSFetchRequest *)fetchRequest managedObjectContext:(NSManagedObjectContext *)context;
- (instancetype)initWithFetchRequest:(NSFetchRequest *)fetchRequest managedObjectContext:(NSManagedObjectContext *)context estimatedMaxCapacity:(NSUInteger)capacity options:(UTFetchedObjectsCountControllerMode)mode NS_DESIGNATED_INITIALIZER;
- (BOOL)performFetch:(NSError **)error;

- (NSUInteger)count;

@end

@protocol UTFetchedObjectsCountControllerDelegate <NSObject>

@required
- (void)objectsCountControllerDidChangeCount:(UTFetchedObjectsCountController *)controller;

@end

