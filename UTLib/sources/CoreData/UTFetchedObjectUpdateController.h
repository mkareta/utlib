//
//  UTFetchedObjectUpdateController.h
//  UTLib
//
//  Created by Max Kareta on 12/2/15.
//  Copyright © 2015 Max Kareta. All rights reserved.
//

@class NSManagedObject;
@class NSManagedObjectID;

@protocol UTFetchedObjectUpdateControllerDelegate;

typedef NS_OPTIONS(NSInteger, UTFetchedObjectUpdateControllerOptions)
{
    kUTFetchedObjectUpdateControllerReportNew = 1 << 0,
    kUTFetchedObjectUpdateControllerReportOld = 1 << 1,
    kUTFetchedObjectUpdateControllerUseObjectSnapshot = 1 << 2
};

@interface UTFetchedObjectUpdateController : NSObject

@property (strong, nonatomic) NSArray *attributes;
@property (weak, nonatomic) id <UTFetchedObjectUpdateControllerDelegate> delegate;

- (instancetype)initWithObject:(NSManagedObject *)object attributes:(NSArray *)attributes options:(UTFetchedObjectUpdateControllerOptions)options NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithObject:(NSManagedObject *)object attributes:(NSArray *)attributes;
- (instancetype)initWithObjectID:(NSManagedObjectID *)objectID attributes:(NSArray *)attributes context:(NSManagedObjectContext *)context;
- (instancetype)initWithObjectURI:(NSURL *)URI attributes:(NSArray *)attributes context:(NSManagedObjectContext *)context;

@end

@protocol UTFetchedObjectUpdateControllerDelegate <NSObject>

@required
- (void)objectUpdateControllerObjectWasUpdated:(UTFetchedObjectUpdateController *)controller oldValues:(NSDictionary *)oldValues newValues:(NSDictionary *)newValues;
- (void)objectUpdateControllerObjectWasDeleted:(UTFetchedObjectUpdateController *)controller;

@end

