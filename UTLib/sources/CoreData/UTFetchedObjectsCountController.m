//
//  UTFetchedObjectsCountController.m
//  UTLib
//
//  Created by Max Kareta on 2/13/16.
//  Copyright © 2016 Max Kareta. All rights reserved.
//

@import CoreData;

#import "UTFetchedObjectsCountController.h"

@interface UTFetchedObjectsCountController()
{
@private
    NSInteger _count;
    NSUInteger _maxCapacity;
    BOOL _trackDidChange;
    BOOL _trackSave;
}

@property (strong, nonatomic) NSFetchRequest *fetchRequest;
@property (strong, nonatomic) NSMutableSet *objects;
@property (strong, nonatomic) NSManagedObjectContext *context;


@end

@implementation UTFetchedObjectsCountController

- (instancetype)initWithFetchRequest:(NSFetchRequest *)fetchRequest managedObjectContext:(NSManagedObjectContext *)context
{
    return [self initWithFetchRequest:fetchRequest managedObjectContext:context estimatedMaxCapacity:0 options:kUTFetchedObjectsCountControllerUpdateOnChange];
}

- (instancetype)initWithFetchRequest:(NSFetchRequest *)fetchRequest managedObjectContext:(NSManagedObjectContext *)context estimatedMaxCapacity:(NSUInteger)capacity options:(UTFetchedObjectsCountControllerMode)mode
{
    self = [super init];
    if (nil != self)
    {
        NSFetchRequest *newFetchRequest = [NSFetchRequest fetchRequestWithEntityName:fetchRequest.entityName];
        newFetchRequest.predicate = fetchRequest.predicate;
        newFetchRequest.includesPropertyValues = NO;
        newFetchRequest.includesSubentities = fetchRequest.includesSubentities;
        _fetchRequest = newFetchRequest;
        _context = context;
        _maxCapacity = capacity;
        _trackDidChange = mode == kUTFetchedObjectsCountControllerUpdateOnChange;
        _trackSave = mode == kUTFetchedObjectsCountControllerUpdateOnContextSave;
        if (_trackDidChange)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentContextDidChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.context];
        }
        if (_trackSave)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentContextDidSave:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.context];
        }
    }
    return self;
}

- (BOOL)performFetch:(NSError *__autoreleasing*)error
{
    NSArray *ids = [self.context executeFetchRequest:self.fetchRequest error:error];
    if (nil != ids)
    {
        NSUInteger capacity = MAX((NSUInteger)(ids.count * 1.4), _maxCapacity);
        _maxCapacity = capacity;
        self.objects = [[NSMutableSet alloc] initWithCapacity:_maxCapacity];
        [self.objects addObjectsFromArray:ids];
        _count = ids.count;
        return YES;
    }
    else
    {
        return NO;
    }
}

- (NSUInteger)count
{
    return _count;
}

- (void)processInserted:(NSSet *)inserted updated:(NSSet *)updated deleted:(NSSet *)deleted
{
    for (NSManagedObject *object in deleted)
    {
        [self.objects removeObject:object];
    }
    for (NSManagedObject *object in inserted)
    {
        BOOL passed = [_fetchRequest.predicate evaluateWithObject:object];
        if (passed)
        {
            [self.objects addObject:object];
        }
    }
    for (NSManagedObject *object in updated)
    {
        BOOL passed = [_fetchRequest.predicate evaluateWithObject:object];
        BOOL inSet = [self.objects containsObject:object];
        if (passed && !inSet)
        {
            [self.objects addObject:object];
        }
        else if (!passed && inSet)
        {
            [self.objects removeObject:object];
        }
    }
}

- (void)processNotification:(NSNotification *)notification
{
    NSSet *inserted = notification.userInfo[NSInsertedObjectsKey];
    NSSet *update = notification.userInfo[NSUpdatedObjectsKey];
    NSSet *deleted = notification.userInfo[NSDeletedObjectsKey];
    [self processInserted:inserted updated:update deleted:deleted];
}

- (void)currentContextDidChange:(NSNotification *)notification
{
    [self processNotification:notification];
}

- (void)currentContextDidSave:(NSNotification *)notificaiton
{
    [self processNotification:notificaiton];
}

@end
