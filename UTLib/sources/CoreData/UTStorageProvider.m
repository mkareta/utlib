//
//  UTStorageProvider.m
//  UTLib
//
//  Created by Max Kareta on 12/13/15.
//  Copyright © 2015 Max Kareta. All rights reserved.
//

@import CoreData;

#import "UTStorageProvider.h"

@interface UTStorageProvider()

@property (strong, nonatomic) NSRecursiveLock *lock;
@property (strong, nonatomic) NSManagedObjectModel *model;
@property (strong, nonatomic) NSPersistentStoreCoordinator *coordinator;
@property (strong, nonatomic) NSPersistentStoreCoordinator *backgroundCoordinator;
@property (strong, nonatomic) NSManagedObjectContext *mainUIContext;
@property (strong, nonatomic) NSManagedObjectContext *mainBackgroundContext;


@end

@implementation UTStorageProvider

static BOOL sDeleteFilesOnSetup = NO;

+ (void)setNeedsDeleteDatabaseFilesOnSetup
{
    sDeleteFilesOnSetup = YES;
}

- (instancetype)init
{
    self = [super init];
    if (nil != self)
    {
        self.lock = [NSRecursiveLock new];
        if (sDeleteFilesOnSetup)
        {
            [self deleteDatabaseFiles];
        }
        [self setupDefaultStack];
    }
    return self;
}

- (NSString *)modelName
{
    NSAssert(NO, @"");
    return nil;
}

- (NSString *)fileName
{
    return @"database";
}

- (NSString *)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [NSString stringWithFormat:@"%@/%@.%@", paths[0], self.fileName, @"sqlite"];
    return path;
}

- (BOOL)deleteDatabaseOnSetupError
{
    return YES;
}

- (NSString *)protectionFlag
{
    return NSFileProtectionComplete;
}

- (NSDictionary *)openOptions
{
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES,
                              NSInferMappingModelAutomaticallyOption: @YES,
                              NSSQLitePragmasOption: @{@"journal_mode": @"WAL"},
                              NSPersistentStoreFileProtectionKey: self.protectionFlag};
    return options;
}

- (NSURL *)modelURL
{
    return [[NSBundle bundleForClass:[self class]] URLForResource:self.modelName withExtension:@"momd"];
}

- (BOOL)deleteFileAtPath:(NSString *)path error:(NSError *__autoreleasing *)error
{
    return [[NSFileManager defaultManager] removeItemAtPath:path error:error];
}

- (void)deleteDatabaseFiles
{
    [self deleteFileAtPath:[self filePath] error:NULL];
}

- (BOOL)handleOpenError:(NSError *)error
{
    if ([self deleteDatabaseOnSetupError])
    {
        [self deleteDatabaseFiles];
        return YES;
    }
    return NO;
}

- (void)didFailToSetupWithError:(NSError *)error
{
    
}

- (void)setupDefaultStackIsRetry:(BOOL)retry
{
    NSDictionary *options = [self openOptions];
    NSURL *modelURL = [self modelURL];
    self.model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    NSURL *storeURL = [NSURL fileURLWithPath:self.filePath isDirectory:NO];
    
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    self.backgroundCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    NSError *error = nil;
    NSPersistentStore *persistentStore = [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
    if (nil == persistentStore)
    {
        if (!retry && [self handleOpenError:error])
        {
            [self setupDefaultStackIsRetry:YES];
        }
        else
        {
            [self didFailToSetupWithError:error];
        }
    }
    else
    {
        [self.backgroundCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
        if (nil != error)
        {
            [self didFailToSetupWithError:error];
        }
        else
        {
            self.mainUIContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            self.mainUIContext.persistentStoreCoordinator = self.coordinator;
            self.mainUIContext.mergePolicy = [self UIMergePolicy];
            self.mainBackgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            self.mainBackgroundContext.persistentStoreCoordinator = self.backgroundCoordinator;
            self.mainBackgroundContext.mergePolicy = [self backgroundMergePolicy];
        }
    }
}

- (void)setupDefaultStack
{
    [self setupDefaultStackIsRetry:NO];
}


- (NSMergePolicy *)UIMergePolicy
{
    return NSMergeByPropertyStoreTrumpMergePolicy;
}

- (NSMergePolicy *)backgroundMergePolicy
{
    return NSMergeByPropertyObjectTrumpMergePolicy;
}

- (NSManagedObjectContext *)newRootMainContextWithMergePolicy:(NSMergePolicy *)policy
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    context.mergePolicy = policy;
    context.persistentStoreCoordinator = self.coordinator;
    return context;
}

- (NSManagedObjectContext *)newRootBackgroundContextWithMergePolicy:(NSMergePolicy *)policy
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.mergePolicy = policy;
    context.persistentStoreCoordinator = self.backgroundCoordinator;
    return context;
}

- (NSManagedObjectContext *)newBackgroundContext
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.parentContext = [self mainBackgroundContext];
    context.undoManager = nil;
    return context;
}

- (void)performFetchWithBlock:(UTBlockWithContext)block
{
    NSManagedObjectContext *context = [self newBackgroundContext];
    [context performBlock:^{
        block(context);
        [context reset];
    }];
}

- (void)performBackgroundTaskWithBlock:(UTBlockWithContext)block
{
    NSManagedObjectContext *context = [self newBackgroundContext];
    [context performBlock:^{
        block(context);
        [self saveContext:context saveParent:YES];
        [context reset];
    }];
}

- (void)performChangesForObject:(NSManagedObject *)object withBlock:(UTBlockWithContextAndArray)block
{
    [self performChangesForObjects:@[object] withBlock:block];
}

- (void)performChangesForObjects:(NSArray *)objects withBlock:(UTBlockWithContextAndArray)block
{
    NSManagedObjectContext *parent = [objects.firstObject managedObjectContext];
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.parentContext = parent;
    [self performChangesForObjects:objects inContext:context withBlock:block];
}

- (void)performChangesForObjects:(NSArray *)objects inContext:(NSManagedObjectContext *)context withBlock:(UTBlockWithContextAndArray)block
{
    NSString *entityName = [[objects.firstObject entity] name];
    NSAssert(objects.count == 0 || [[NSSet setWithArray:[objects valueForKey:@"managedObjectContext"]] count] == 1, @"All objects should use same context");
    NSAssert(objects.count == 0 || [[NSSet setWithArray:[objects valueForKey:@"entity"]] count] == 1, @"Array contains objects of different types");
    __block NSArray *objectIDs = [objects valueForKey:@"objectID"];
    [context performBlock:^{
        NSArray *result = @[];
        if (objectIDs.count > 0) {
            NSManagedObjectID *objectID = objectIDs.firstObject;
            if (objectID.persistentStore != context.persistentStoreCoordinator.persistentStores.firstObject) {
                NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:[objectIDs count]];
                for (NSManagedObjectID *tempObjectID in objectIDs) {
                    [tempArray addObject:[context.persistentStoreCoordinator managedObjectIDForURIRepresentation:tempObjectID.URIRepresentation]];
                }
            }
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
            request.predicate = [NSPredicate predicateWithFormat:@"self IN %@", objectIDs];
            result = [context executeFetchRequest:request error:nil];
        }
        block(context, result);
    }];
}

- (void)willSaveContext:(NSManagedObjectContext *)context
{
    
}

- (void)didFailToSaveContext:(NSManagedObjectContext *)context withError:(NSError *)error
{
    NSAssert(NO, @"Fail to save context: %@ with error: %@", context, error);
}

- (BOOL)recoverFromSaveError:(NSError *)error context:(NSManagedObjectContext *)context
{
    return NO;
}

- (NSError *)saveContext:(NSManagedObjectContext *)context saveParent:(BOOL)saveParent
{
    return [self saveContext:context saveParent:saveParent completionBLock:NULL];
}

- (NSError *)saveContext:(NSManagedObjectContext *)context saveParent:(BOOL)saveParent completionBLock:(UTEmptyBlock)block
{
    BOOL saveSucceeded = YES;
    NSError *error = nil;
    if ([context hasChanges])
    {
        [self willSaveContext:context];
        saveSucceeded = [context save:&error];
        if (!saveSucceeded && nil != error)
        {
            if ([self recoverFromSaveError:error context:context])
            {
                error = nil;
                saveSucceeded = [context save:&error];
            }
        }
        if (!saveSucceeded)
        {
            [self didFailToSaveContext:context withError:error];
        }
    }
    if (saveSucceeded && saveParent && nil != context.parentContext)
    {
        NSManagedObjectContext *parentContext = context.parentContext;
        [parentContext performBlock:^{
            [self saveContext:parentContext saveParent:saveParent completionBLock:block];
        }];
    }
    else
    {
        UTInvokeBlock(block);
    }
    return error;
}

- (NSError *)saveContext:(NSManagedObjectContext *)context
{
    return [self saveContext:context saveParent:YES];
}

- (NSError *)saveContext:(NSManagedObjectContext *)context completionBLock:(UTEmptyBlock)block
{
    return [self saveContext:context saveParent:YES completionBLock:block];
}

@end
