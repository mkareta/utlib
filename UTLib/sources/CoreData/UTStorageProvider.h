//
//  UTStorageProvider.h
//  UTLib
//
//  Created by Max Kareta on 12/13/15.
//  Copyright © 2015 Max Kareta. All rights reserved.
//

typedef void (^UTBlockWithContext)(NSManagedObjectContext *context);
typedef void (^UTBlockWithContextAndArray)(NSManagedObjectContext *context, NSArray *objects);

@class NSMergePolicy;
@class NSManagedObjectContext;
@class NSManagedObject;

#import "GCD+UTFunctions.h"

@class NSMergePolicy;
@class NSManagedObject;

@interface UTStorageProvider : NSObject

+ (void)setNeedsDeleteDatabaseFilesOnSetup;

- (instancetype)init NS_DESIGNATED_INITIALIZER;
- (NSString *)modelName;
- (NSString *)filePath;
- (NSString *)fileName;
- (BOOL)deleteDatabaseOnSetupError;
- (NSDictionary *)openOptions;
- (NSString *)protectionFlag;
- (NSURL *)modelURL;
- (void)deleteDatabaseFiles;
- (BOOL)deleteFileAtPath:(NSString *)path error:(NSError **)error;
- (BOOL)handleOpenError:(NSError *)error;
- (void)didFailToSetupWithError:(NSError *)error;

- (NSMergePolicy *)UIMergePolicy;
- (NSMergePolicy *)backgroundMergePolicy;

- (NSManagedObjectContext *)mainUIContext;
- (NSManagedObjectContext *)mainBackgroundContext;
- (NSManagedObjectContext *)newRootMainContextWithMergePolicy:(NSMergePolicy *)policy;
- (NSManagedObjectContext *)newRootBackgroundContextWithMergePolicy:(NSMergePolicy *)policy;

- (NSManagedObjectContext *)newBackgroundContext;

- (void)willSaveContext:(NSManagedObjectContext *)context;
- (BOOL)recoverFromSaveError:(NSError *)error context:(NSManagedObjectContext *)context;
- (void)didFailToSaveContext:(NSManagedObjectContext *)context withError:(NSError *)error;

- (void)performFetchWithBlock:(UTBlockWithContext)block;
- (void)performBackgroundTaskWithBlock:(UTBlockWithContext)block;
- (void)performChangesForObject:(NSManagedObject *)object withBlock:(UTBlockWithContextAndArray)block;
- (void)performChangesForObjects:(NSArray *)objects withBlock:(UTBlockWithContextAndArray)block;

- (void)performChangesForObjects:(NSArray *)objects inContext:(NSManagedObjectContext *)context withBlock:(UTBlockWithContextAndArray)block;

- (NSError *)saveContext:(NSManagedObjectContext *)context saveParent:(BOOL)saveParent;
- (NSError *)saveContext:(NSManagedObjectContext *)context saveParent:(BOOL)saveParent completionBLock:(UTEmptyBlock)block;
- (NSError *)saveContext:(NSManagedObjectContext *)context;
- (NSError *)saveContext:(NSManagedObjectContext *)context completionBLock:(UTEmptyBlock)block;

@end
