//
//  NSEnumerator+UTMethods.m
//  UTLib
//
//  Created by Max Kareta on 27/12/2017.
//  Copyright © 2017 Max Kareta. All rights reserved.
//

#import "NSEnumerator+UTMethods.h"

@implementation NSEnumerator(UTMethods)

- (NSArray *)map:(id (^)(id object))block {
    NSMutableArray *result = [NSMutableArray new];
    id nextObject = self.nextObject;
    while (nextObject) {
        [result addObject:block(nextObject)];
        nextObject = self.nextObject;
    }
    return result;
}

- (NSArray *)filter:(BOOL (^)(id object))filter andMap:(id (^)(id object))map {
    NSMutableArray *result = [NSMutableArray new];
    id nextObject = self.nextObject;
    while (nextObject) {
        if (filter(nextObject)) {
            [result addObject:map(nextObject)];
        }        
        nextObject = self.nextObject;
    }
    return result;
}

- (NSArray *)mapAndFilterNulls:(id (^)(id object))block {
    NSMutableArray *result = [NSMutableArray new];
    id nextObject = self.nextObject;
    while (nextObject) {
        id mapObject = block(nextObject);
        if (mapObject) {
            [result addObject:mapObject];
        }
        nextObject = self.nextObject;
    }
    return result;
}

- (NSArray *)filter:(BOOL (^)(id object))block {
    NSMutableArray *result = [NSMutableArray new];
    id nextObject = self.nextObject;
    while (nextObject) {
        if (block(nextObject)) {
            [result addObject:nextObject];
        }
        nextObject = self.nextObject;
    }
    return result;
}

- (NSDictionary *)dict:(id<NSCopying> (^)(id object))block {
    NSMutableDictionary *result = [NSMutableDictionary new];
    id nextObject = self.nextObject;
    while (nextObject) {
        id key = block(nextObject);
        result[key] = nextObject;
        nextObject = self.nextObject;
    }
    return result;
}

@end
