//
//  GCD+UTFunctions.m
//  UTLib
//
//  Created by Maksym Kareta on 10/15/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "GCD+UTFunctions.h"

void dispatch_with_delay(NSTimeInterval delay, dispatch_block_t block)
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

void dispatch_async_on_main_queue(dispatch_block_t block)
{
    dispatch_async(dispatch_get_main_queue(), block);
}
