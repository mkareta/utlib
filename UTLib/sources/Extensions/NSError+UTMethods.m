//
//  NSError+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 12/29/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "NSError+UTMethods.h"

@implementation NSError(UTMethods)

- (BOOL)isInDomain:(NSString *)domain
{
    return [self.domain isEqualToString:domain];
}

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code
{
    return [self errorWithDomain:domain code:code userInfo:nil];
}

+ (id)errorWithDomain:(NSString *)domain code:(NSInteger)code underlyingError:(NSError *)error
{
    if (error == nil)
    {
        return [self errorWithDomain:domain code:code];
    }
    return [self errorWithDomain:domain code:code userInfo:@{NSUnderlyingErrorKey : error}];
}

@end
