//
//  NSError+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 12/29/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError(UTMethods)

- (BOOL)isInDomain:(NSString *)domain;

///Creates and initializes an NSError object for a given domain and code with a given userInfo dictionary.
///@param domain The error domain—this can be one of the predefined NSError domains, or an arbitrary string describing a custom domain. domain must not be nil. See “Error Domains” for a list of predefined domains.
///@param code The error code for the error.
///@return An NSError object for domain with the specified error code and the dictionary of arbitrary data userInfo.
+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code;

///Creates and initializes an NSError object for a given domain and code with a given userInfo dictionary.
///@param domain The error domain—this can be one of the predefined NSError domains, or an arbitrary string describing a custom domain. domain must not be nil. See “Error Domains” for a list of predefined domains.
///@param code The error code for the error.
///@param error The underlying error that will be passed to user info dictionary. Error may be nil.
///@return An NSError object for domain with the specified error code and the dictionary of arbitrary data userInfo.
+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code underlyingError:(NSError *)error;

@end
