//
//  NSArray+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 6/11/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSArray+UTMethods.h"
#import "NSEnumerator+UTMethods.h"

@implementation NSArray (UTMethods)

- (void)makeObjectsPerformSelector:(SEL)aSelector withObject:(id)firstObject secondObject:(id)secondObject
{
    [self enumerateObjectsUsingBlock:^(NSObject *obj, NSUInteger idx, BOOL *stop) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [obj performSelector:aSelector withObject:firstObject withObject:secondObject];
#pragma clang diagnostic pop
    }];
}

- (NSArray *)sortedArrayUsingDescriptor:(NSSortDescriptor *)desciptor
{
    return [self sortedArrayUsingDescriptors:@[desciptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key ascending:(BOOL)ascending
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key
{
    return [self sortedArrayUsingKey:key ascending:YES];
}

- (NSArray *)map:(id (^)(id object))block {
    return [self.objectEnumerator map:block];
}

- (NSArray *)mapAndFlatten:(id (^)(id object))block {
    return [[self map:block] flatten];
}

- (NSDictionary *)dict:(id<NSCopying> (^)(id object))block {
    return [self.objectEnumerator dict:block];
}

- (NSArray *)filter:(BOOL (^)(id object))block {
    return [self.objectEnumerator filter:block];
}

- (NSArray *)mapAndFilterNulls:(id (^)(id object))block {
    return [self.objectEnumerator mapAndFilterNulls:block];
}

- (NSArray *)flatten {
    NSMutableArray *result = [NSMutableArray new];
    for (id object in self) {
        if ([object isKindOfClass:[NSArray class]]) {
            [result addObjectsFromArray:[object flatten]];
        } else {
            [result addObject:object];
        }
    }
    return result;
}

+ (NSArray *)merge:(NSArray<NSArray *> *)arrays {
    NSMutableArray *result = [NSMutableArray new];
    for (NSArray *subArray in arrays) {
        [result addObjectsFromArray:subArray];
    }
    return result;
}

+ (NSArray *)mergeAndFlatten:(NSArray<NSArray *> *)arrays {
    return [[self merge:arrays] flatten];
}

@end

@implementation NSMutableArray (UTMethods)

- (void)addObjectIfNotNil:(id)obj
{
    if (nil != obj)
    {
        [self addObject:obj];
    }
}

- (void)sortArrayUsingDescriptor:(NSSortDescriptor *)desciptor
{
    [self sortUsingDescriptors:@[desciptor]];
}

- (void)sortArrayUsingKey:(NSString *)key ascending:(BOOL)ascending
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    [self sortUsingDescriptors:@[descriptor]];
}

- (void)sortArrayUsingKey:(NSString *)key
{
    [self sortArrayUsingKey:key ascending:YES];
}

@end
