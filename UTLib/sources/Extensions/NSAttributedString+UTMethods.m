//
//  NSAttributedString+UTMethods.m
//  UTLib
//
//  Created by Max Kareta on 6/27/15.
//  Copyright (c) 2015 Max Kareta. All rights reserved.
//

#import "NSAttributedString+UTMethods.h"

@implementation NSAttributedString(UTMethods)

+ (NSAttributedString *)attributedStringWithFirstPart:(NSString *)first color:(UIColor *)color secondPart:(NSString *)second color:(UIColor *)color2 font:(UIFont *)font
{
    NSString *tempString = [NSString stringWithFormat:@"%@%@", first, second];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:tempString];
    [string setAttributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : color}
                    range:NSMakeRange(0, first.length)];
    [string setAttributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : color2}
                    range:NSMakeRange(first.length, tempString.length - first.length)];
    return string;
}

+ (NSAttributedString *)attributedStringWithString:(NSString *)string color:(UIColor *)color font:(UIFont *)font
{
    return [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : color}];
}

@end
