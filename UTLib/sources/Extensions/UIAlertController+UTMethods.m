//
//  UIAlertController+UTMethods.m
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UIAlertController+UTMethods.h"

@implementation UIAlertController(UTMethods)

- (UIAlertAction *)addActionWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^ __nullable)(UIAlertAction *action))handler
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:style handler:handler];
    [self addAction:action];
    return action;
}

- (UIAlertAction *)addDefaultActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler
{
    return [self addActionWithTitle:title style:UIAlertActionStyleDefault handler:handler];
}

- (UIAlertAction *)addCancelActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler
{
    return [self addActionWithTitle:title style:UIAlertActionStyleCancel handler:handler];
}

- (UIAlertAction *)addDestructiveActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler
{
    return [self addActionWithTitle:title style:UIAlertActionStyleDestructive handler:handler];
}

@end
