//
//  CGGeometry+UTExtensions.h
//  UTLib
//
//  Created by Ivan Cherniukh on 04.03.13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

CG_INLINE CGPoint UTHomotheticTransformedPoint(CGPoint point, CGPoint homotheticCenter)
{
	CGPoint retValue = point;
	retValue.x -= 2.0 * (point.x - homotheticCenter.x);
	retValue.y -= 2.0 * (point.y - homotheticCenter.y);
	return retValue;
}
