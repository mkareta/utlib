//
//  UIColor+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 10/15/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

@interface UIColor(UTMethods)

- (NSNumber *)RGBAHexNumber;
- (u_int32_t)RGBAHexValue;

@end

/// Creates and returns a UIColor object using the specified RGB component values.
/// @param r The red component of the color object, specified as a value from 0 to 255.
/// @param g The green component of the color object, specified as a value from 0 to 255.
/// @param b The blue component of the color object, specified as a value from 0 to 255.
/// @return The color object. The color information represented by this object is in the device RGB colorspace.
FOUNDATION_EXTERN UIColor *UTRGBColor(NSInteger r, NSInteger g, NSInteger b);

/// Creates and returns a UIColor object using the specified RGBA component values.
/// @param r The red component of the color object, specified as a value from 0 to 255.
/// @param g The green component of the color object, specified as a value from 0 to 255.
/// @param b The blue component of the color object, specified as a value from 0 to 255.
/// @param a The alpha component of the color object, specified as a value from 0 to 255.
/// @return The color object. The color information represented by this object is in the device RGBA colorspace.
FOUNDATION_EXTERN UIColor *UTRGBAColor(NSInteger r, NSInteger g, NSInteger b, NSInteger a);

/// Creates and returns a UIColor object using the specified RGBA hex value.
/// @param hex The hex representation of color.
/// @return The color object. The color information represented by this object is in the device RGBA colorspace.
FOUNDATION_EXTERN UIColor *UTRGBAColorWithHex(uint32_t hex);

/// Creates and returns a UIColor object using the specified RGBA hex value.
/// @param number The hex representation of color.
/// @return The color object. The color information represented by this object is in the device RGBA colorspace. Return nil if number is nil
FOUNDATION_EXTERN UIColor *UTRGBAColorWithHexNumber(NSNumber *number);
