//
//  NSMutableString+UTMethods.m
//  UTLib
//
//  Created by Max Kareta on 5/21/16.
//  Copyright © 2016 Max Kareta. All rights reserved.
//

#import "NSMutableString+UTMethods.h"

@implementation NSMutableString(UTMethods)

- (void)appendStringUsingSpace:(NSString *)aString
{
    [self appendString:aString spacer:@" "];
}

- (void)appendStringEmptySpacer:(NSString *)aString
{
    [self appendString:aString spacer:nil];
}

- (void)appendString:(NSString *)aString spacer:(NSString *)spacer
{
    if (aString.length > 0)
    {
        if (self.length > 0 && spacer.length > 0)
        {
            [self appendString:spacer];
        }
        [self appendString:aString];
    }
}

@end
