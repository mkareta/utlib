//
//  NSDictionary+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 12/29/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "NSDictionary+UTMethods.h"

@implementation NSDictionary(UTMethods)

- (BOOL)boolValueForKey:(id)key defaultValue:(BOOL)defaultValue
{
    id object = [self objectForKey:key];
    return nil != object ? [object boolValue] : defaultValue;
}

@end
