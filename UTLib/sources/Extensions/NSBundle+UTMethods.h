//
//  NSBundle+UTMethods.h
//  UTLib
//
//  Created by Olena Kachura on 11/24/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle(UTMethods)

///Convenience mehtod for loading nibs.
///@discussion Use main bundle as bundle and nil as options.
///@see - [NSBundle loadNibNamed:owner:options:].
+ (NSArray *)loadNibNamed:(NSString *)name owner:(id)owner;

@end
