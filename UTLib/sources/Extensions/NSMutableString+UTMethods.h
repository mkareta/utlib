//
//  NSMutableString+UTMethods.h
//  UTLib
//
//  Created by Max Kareta on 5/21/16.
//  Copyright © 2016 Max Kareta. All rights reserved.
//

@interface NSMutableString(UTMethods)

- (void)appendStringUsingSpace:(NSString *)aString;
- (void)appendStringEmptySpacer:(NSString *)aString;
- (void)appendString:(NSString *)aString spacer:(NSString *)spacer;

@end
