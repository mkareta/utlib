//
//  NSEnumerator+UTMethods.h
//  UTLib
//
//  Created by Max Kareta on 27/12/2017.
//  Copyright © 2017 Max Kareta. All rights reserved.
//

@import Foundation;

@interface NSEnumerator(UTMethods)

- (NSArray *)filter:(BOOL (^)(id object))block andMap:(id (^)(id object))block;
- (NSArray *)map:(id (^)(id object))block;
- (NSArray *)mapAndFilterNulls:(id (^)(id object))block;
- (NSArray *)filter:(BOOL (^)(id object))block;
- (NSDictionary *)dict:(id<NSCopying> (^)(id object))block;

@end
