//
//  NSAttributedString+UTMethods.h
//  UTLib
//
//  Created by Max Kareta on 6/27/15.
//  Copyright (c) 2015 Max Kareta. All rights reserved.
//

@interface NSAttributedString(UTMethods)

+ (NSAttributedString *)attributedStringWithFirstPart:(NSString *)first color:(UIColor *)color secondPart:(NSString *)second color:(UIColor *)color2 font:(UIFont *)font;
+ (NSAttributedString *)attributedStringWithString:(NSString *)string color:(UIColor *)color font:(UIFont *)font;

@end
