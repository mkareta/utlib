//
//  NSData+UTMethods.h
//  UTLib
//
//  Created by Max Kareta on 4/30/15.
//  Copyright (c) 2015 Max Kareta. All rights reserved.
//

@interface NSData(UTMethods)

+ (NSData *)randomDataWithLength:(NSUInteger)length;

@end
