//
//  UIDevice+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 12/11/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UIDevice+UTMethods.h"

@implementation UIDevice(UTMethods)

+ (BOOL)isIOS7orHigher
{
    static BOOL result = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        result = [[[UIDevice currentDevice] systemVersion] floatValue] >= 7;
    });
    return result;
}

+ (BOOL)isIOS8orHigher
{
    static BOOL result = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        result = [[[UIDevice currentDevice] systemVersion] floatValue] >= 8;
    });
    return result;
}

@end
