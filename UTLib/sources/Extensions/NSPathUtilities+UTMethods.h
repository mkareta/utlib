//
//  NSPathUtilities+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 7/21/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

FOUNDATION_EXPORT NSString *UTDocumentsPathWithComponent(NSString *component);
