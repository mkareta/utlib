//
//  UIDevice+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 12/11/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

@interface UIDevice(UTMethods)

+ (BOOL)isIOS7orHigher;
+ (BOOL)isIOS8orHigher;

@end
