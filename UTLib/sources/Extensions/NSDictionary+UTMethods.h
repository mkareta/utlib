//
//  NSDictionary+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 12/29/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

@interface NSDictionary(UTMethods)

///Return bool value for key using - (BOOL)boolValue method and defaultValue
/// if object don't exists
///@param key The key for which to return the corresponding value.
///@param defaultValue The default value that will be returned if object don't exists
///@return result of calling boolValue method or defaultValue
- (BOOL)boolValueForKey:(id)key defaultValue:(BOOL)defaultValue;

@end


