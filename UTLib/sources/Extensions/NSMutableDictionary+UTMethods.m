//
//  NSMutableDictionary+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 5/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "NSMutableDictionary+UTMethods.h"

@implementation NSMutableDictionary(UTMethods)

- (void)safeSetObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    if (anObject != nil)
    {
        [self setObject:anObject forKey:aKey];
    }
}

@end
