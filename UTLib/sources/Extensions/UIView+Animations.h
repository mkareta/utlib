//
//  UIView+animations.h
//  UTLib
//
//  Created by Maksym Kareta on 4/29/12.
//  Copyright (c) 2012 Maksym Kareta, Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIViewUTAnimationsSequenceBlock)(void);
typedef void (^UIViewUTAnimationsSequenceCompletionBlock)(void);

@interface UIView (UTAnimations)

- (void)addAnimationBlock:(UIViewUTAnimationsSequenceBlock)block duration:(NSTimeInterval)duration;
- (void)addAnimationBlock:(UIViewUTAnimationsSequenceBlock)block completion:(UIViewUTAnimationsSequenceCompletionBlock)completion duration:(NSTimeInterval)duration;
- (void)commitAnimationSequenceWithCompletionBlock:(UIViewUTAnimationsSequenceCompletionBlock)block;
- (void)commitAnimationSequence;

@end
