//
//  UIAlertController+UTMethods.h
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController(UTMethods)

- (UIAlertAction *)addActionWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^ __nullable)(UIAlertAction *action))handler;
- (UIAlertAction *)addDefaultActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler;
- (UIAlertAction *)addCancelActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler;
- (UIAlertAction *)addDestructiveActionWithTitle:(NSString *)title handler:(void (^ __nullable)(UIAlertAction *action))handler;

@end
