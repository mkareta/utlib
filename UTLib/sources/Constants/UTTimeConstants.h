//
//  UTTimeConstants.h
//  UTLib
//
//  Created by Maksym Kareta on 6/2/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

static const NSTimeInterval kUTMinute = 60;
static const NSTimeInterval kUTHour = 60 * kUTMinute;
static const NSTimeInterval kUTDay = 24 * kUTHour;
static const NSTimeInterval kUTWeek = 7 * kUTDay;


