//
//  UTDictionaryTransformMap.h
//  UTLib
//
//  Created by Maksym Kareta on 12/30/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

///Transform block.
///@param object Object that will be transformed.
///@param key Unique key.
///@return Dictionary with info that should be added to result dictionary.
///@warning Never use self as reference to object.
typedef NSDictionary * (^UTTransformMapBlock)(id object, NSString *key);

///Object update block.
///@param object Object that should be updated.
///@param dict Dictionary representation of object.
///@param key Unique key.
///@warning Never use self as reference to object.
typedef void (^UTTransformMapUpdateBlock)(id object, NSString *key, NSDictionary *dict);

///Object transform map.
@interface UTDictionaryTransformMap : NSObject

///@param key Object key/keypath. Same key will be used in result dictionary.
- (void)addKey:(NSString *)key;

///@param key1 Object key/keypath.
///@param key2 Result dictionary key/keypath. Will create sub dictionaries in case when key2 is keypath.
- (void)addObjectKey:(NSString *)key1 dictionaryKey:(NSString *)key2;

///@param key Unique key.
///@param block1 UTTransformMapBlock that return dictionary with info for key.
///@param block2 UTTransformMapUpdateBlock that should update provided object with info from dictionary according to key.
- (void)addObjectKey:(NSString *)key dictionaryRepresentation:(UTTransformMapBlock)block1 updateObject:(UTTransformMapUpdateBlock)block2;

///@param key Unique key.
///@param key2 Result dictionary key/keypath. Will create sub dictionaries in case when key2 is keypath.
///@param block UTTransformMapBlock that return dictionary with info for key.
- (void)addObjectKey:(NSString *)key dictionaryKey:(NSString *)key2 dictionaryRepresentation:(UTTransformMapBlock)block;

///@param key Unique key.
///@param key2 Result dictionary key/keypath. Will create sub dictionaries in case when key2 is keypath.
///@param block UTTransformMapUpdateBlock that should update provided object with info from dictionary according to key.
- (void)addObjectKey:(NSString *)key dictionaryKey:(NSString *)key2 updateObject:(UTTransformMapUpdateBlock)block;

@end
