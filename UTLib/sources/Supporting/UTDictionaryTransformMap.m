//
//  UTDictionaryTransformMap.m
//  UTLib
//
//  Created by Maksym Kareta on 12/30/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTDictionaryTransformMap.h"
#import "UTDictionaryTransformMap_private.h"

@interface UTDictionaryTransformMap()

@property (strong, nonatomic)  NSMutableArray *map;

@end

@implementation UTDictionaryTransformMap

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        _map = [NSMutableArray array];
    }
    return self;
}

- (void)addObjectKey:(NSString *)key1 dictionaryKey:(NSString *)key2
{
    NSAssert(nil != key1 && nil != key2, @"");
    UTDictionaryTransformMapEntry *entry = [UTDictionaryTransformMapEntry new];
    entry.objectKey = key1;
    entry.dictKey = key2;
    [self.map addObject:entry];
}

- (void)addObjectKey:(NSString *)key dictionaryRepresentation:(UTTransformMapBlock)block1 updateObject:(UTTransformMapUpdateBlock)block2
{
    UTDictionaryTransformMapEntry *entry = [UTDictionaryTransformMapEntry new];
    entry.objectKey = key;
    entry.transform = block1;
    entry.update = block2;
    [self.map addObject:entry];
}

- (void)addKey:(NSString *)key
{
    NSAssert(nil != key, @"");
    UTDictionaryTransformMapEntry *entry = [UTDictionaryTransformMapEntry new];
    entry.objectKey = key;
    entry.dictKey = key;
    [self.map addObject:entry];
}

- (void)addObjectKey:(NSString *)key dictionaryKey:(NSString *)key2 dictionaryRepresentation:(UTTransformMapBlock)block
{
    UTDictionaryTransformMapEntry *entry = [UTDictionaryTransformMapEntry new];
    entry.objectKey = key;
    entry.dictKey = key2;
    entry.transform = block;
    [self.map addObject:entry];
}

- (void)addObjectKey:(NSString *)key dictionaryKey:(NSString *)key2 updateObject:(UTTransformMapUpdateBlock)block
{
    UTDictionaryTransformMapEntry *entry = [UTDictionaryTransformMapEntry new];
    entry.objectKey = key;
    entry.update = block;
    entry.dictKey = key2;
    [self.map addObject:entry];    
}

- (NSArray *)entries
{
    return _map;
}

@end

@implementation UTDictionaryTransformMapEntry

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@> {\nobjectKey:%@\ndictKey:%@\ntransform:%@\nupdate:%@\n}",
            [super description], self.objectKey, self.dictKey, self.transform, self.update];
}

@end

