//
//  UTXIBObjectLoader.m
//  UTLib
//
//  Created by Maksym Kareta on 11/13/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTXIBObjectLoader.h"

@implementation UTXIBObjectLoader

+ (id)loadXIBNamed:(NSString *)name
{
    UTXIBObjectLoader *loader = [UTXIBObjectLoader new];
    NSBundle *bundle = [NSBundle bundleForClass:self];
    [bundle loadNibNamed:name owner:loader options:nil];
    return loader.object;
}

+ (id)loadTopViewFromXib:(NSString *)name
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:name owner:self options:nil];
    return [objects firstObject];
}

@end
