//
//  UTDelegateArray.h
//  UTLib
//
//  Created by Maksym Kareta on 9/3/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "GCD+UTFunctions.h"

@interface UTDelegateArray : NSObject

/* Create and return new UTDelegateArray object.
 *
 * @param object Root object that will always sent as first parameter. Can't be NULL.
 * @return New instance of UTDelegateArray.
 */
- (id)initWithObj:(NSObject *)object;

/* Add delegate to list.
 *
 * @param obj Delegate object.
 * @note UTDelegateArray don't retain "obj" object.
 */
- (void)addDelegate:(NSObject *)obj;

/* Removes delegate from list
 *
 * @param obj Delegate object.
 */
- (void)removeDelegate:(NSObject *)obj;

/* Removes all objects from delegate list
 *
 */
- (void)removeAllObjects;

/* Perfroms selector for all delegates.
 *
 * @param selecter Selector that should be called. Selector style should be selectorName:(id)rootObject.
 * @note Send root object as first parameter.
 * @warning Called methods should not return object. Returing object will cause memory leak.
 */
- (void)delegatePermormSelector:(SEL)selector;

/* Perfroms selector with object for all delegates.
 *
 * @param selecter Selector that should be called. Selector style should be selectorName:(id)rootObject param:(id)param.
 * @note Send root object as first parameter.
 * @warning Called method should not return object. Returing object will cause memory leak.
 */
- (void)delegatePermormSelector:(SEL)selector withObject:(id)objToSend;

/* Enumerate all delegates.
 *
 * @param block Block that will be called for each delegate. Can't be NULL.
 */
- (void)enumerateObjectsUsingBlock:(UTBlockWithObject)block;

@end
