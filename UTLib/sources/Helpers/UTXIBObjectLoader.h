//
//  UTXIBObjectLoader.h
//  UTLib
//
//  Created by Maksym Kareta on 11/13/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

@interface UTXIBObjectLoader : NSObject

@property (strong, nonatomic) IBOutlet id object;

///Load xib into object property and return it.
/// Require UTXIBObjectLoader be object owner
///@param name XIB file name
///@return loaded object
+ (id)loadXIBNamed:(NSString *)name;

///Load xib and return top view
///@param name XIB file name
///@return top view from xib
+ (id)loadTopViewFromXib:(NSString *)name;

@end
