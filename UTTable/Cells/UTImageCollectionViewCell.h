//
//  UTImageCollectionViewCell.h
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

@interface UTImageCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic, readonly) UIImageView *imageView;

@end
