//
//  UTTableViewCell.m
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTableViewCell.h"
#import "GCD+UTFunctions.h"
#import "UTTableTextViewController.h"

@interface UTTableViewCell()

@property (strong, nonatomic) UIView *savedAccessoryView;

@end

@implementation UTTableViewCell

- (BOOL)canPerformAction
{
    return NULL != self.action;
}

- (void)performAction
{
    UTInvokeBlock(self.action, self);
}

- (void)showActivityIndicator
{
    self.savedAccessoryView = self.accessoryView;
    UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view startAnimating];
    self.accessoryView = view;
}

- (void)hideActivityIndicator
{
    self.accessoryView = self.savedAccessoryView;
    self.savedAccessoryView = nil;
}

- (CGFloat)cellHeight
{
    return 44.67;
}

- (void)willDisplay
{

}

- (void)didEndDisplay
{
    
}

- (void)setOverscrollValue:(CGFloat)overscroll
{

}

@end

@implementation UTTableViewController(UTTableViewCell)

- (UTTableViewCell *)addPushCellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block
{
    UTTableViewCell *cell = [self addCellWithText:text actionBlock:block];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UTTableViewCell *)addPushCellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block
{
    UTTableViewCell *cell = [self addCellWithText:text text2:text2 actionBlock:block];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UTTableViewCell *)addPushCellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block
{
    UTTableViewCell *cell = [self addCellWithText:text text2:nil style:UITableViewCellStyleDefault userInfo:userInfo actionBlock:block];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block
{
    return [self addCellWithText:text text2:text2 style:UITableViewCellStyleDefault userInfo:nil actionBlock:block];
}

- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2 style:(UITableViewCellStyle)style userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block
{
    UTTableViewCell *cell = [[UTTableViewCell alloc] initWithStyle:style reuseIdentifier:nil];
    cell.textLabel.text = text;
    cell.detailTextLabel.text = text2;
    cell.userInfo = userInfo;
    cell.action = block;
    if (!cell.action) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return [self addCell:cell];
}

- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2
{
    return [self addCellWithText:text text2:text2 style:UITableViewCellStyleDefault userInfo:nil actionBlock:NULL];
}

- (UTTableViewCell *)addCellWithText:(NSString *)text
{
    return [self addCellWithText:text text2:nil style:UITableViewCellStyleDefault userInfo:nil actionBlock:NULL];
}

- (UTTableViewCell *)addCellWithFormat:(NSString *)text, ...
{
    va_list args;
    va_start(args, text);
    NSString *resultText = [[NSString alloc] initWithFormat:text arguments:args];
    va_end(args);
    return [self addCellWithText:resultText];
}

- (UTTableViewCell *)addCellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block
{
    return [self addCellWithText:text text2:nil style:UITableViewCellStyleDefault userInfo:nil actionBlock:block];
}

- (UTTableViewCell *)addCellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block
{
    return [self addCellWithText:text text2:nil style:UITableViewCellStyleDefault userInfo:userInfo actionBlock:block];
}

- (UTTableViewCell *)addTextViewerCellWithText:(NSString *)text text2:(NSString *)text2 navigationController:(UINavigationController *)nc
{
    UTTableViewCell *cell = [self addCellWithText:text text2:text2 style:UITableViewCellStyleDefault userInfo:nil actionBlock:^(UTTableViewCell *_) {
        UTTableTextViewController *vc = [UTTableTextViewController new];
        [vc setText:text2];
        [nc pushViewController:vc animated:YES];
    }];
    return cell;
}

@end
