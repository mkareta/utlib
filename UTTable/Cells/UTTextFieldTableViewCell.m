//
//  UTTextFieldTableViewCell.m
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTextFieldTableViewCell.h"

@interface UTTextFieldTableViewCell() <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *textField;

@end

@implementation UTTextFieldTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.contentView.autoresizesSubviews = NO;
        self.textField = [[UITextField alloc] init];
        self.textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.textField];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeText:) name:UITextFieldTextDidChangeNotification object:self.textField];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didChangeText:(id)notification
{
    [self performAction];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.textField setFrame:CGRectMake(self.separatorInset.left,
                                        0,
                                        self.contentView.frame.size.width - self.separatorInset.left,
                                        self.contentView.frame.size.height)];
}

@end

@implementation UTTableViewController(UTTextFieldTableViewCell)

- (UTTextFieldTableViewCell *)addTextFieldCellWithPlaceholder:(NSString *)placeholder text:(NSString *)text action:(UTCellActionBlock)action
{
    return [self addTextFieldCellWithPlaceholder:placeholder text:text userInfo:nil action:action];
}

- (UTTextFieldTableViewCell *)addTextFieldCellWithPlaceholder:(NSString *)placeholder text:(NSString *)text userInfo:(id)userInfo action:(UTCellActionBlock)action
{
    UTTextFieldTableViewCell *cell = [[UTTextFieldTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textField.text = text;
    cell.textField.placeholder = placeholder;
    cell.action = action;
    cell.userInfo = userInfo;
    [self addCell:cell];
    return cell;
}

@end
