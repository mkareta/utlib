//
//  UTTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTTableViewController.h"

@class UTTableViewCell;
@class UTSection;

typedef void (^UTCellActionBlock)(id cell);

@interface UTTableViewCell : UITableViewCell

// Valid only when cell on screen. nil when cell is not on display
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) UTSection *section;

@property (strong, nonatomic) id userInfo;
@property (copy, nonatomic) UTCellActionBlock action;

- (CGFloat)cellHeight;

- (BOOL)canPerformAction;
- (void)performAction;

- (void)showActivityIndicator;
- (void)hideActivityIndicator;

- (void)willDisplay;
- (void)didEndDisplay;

- (void)setOverscrollValue:(CGFloat)overscroll;

@end

@interface UTTableViewController(UTTableViewCell)

- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2 style:(UITableViewCellStyle)style userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block;
- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2;
- (UTTableViewCell *)addCellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block;
- (UTTableViewCell *)addCellWithText:(NSString *)text;
- (UTTableViewCell *)addCellWithFormat:(NSString *)text, ...;
- (UTTableViewCell *)addCellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block;
- (UTTableViewCell *)addCellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block;

- (UTTableViewCell *)addPushCellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block;
- (UTTableViewCell *)addPushCellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block;
- (UTTableViewCell *)addPushCellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block;

- (UTTableViewCell *)addTextViewerCellWithText:(NSString *)text text2:(NSString *)text2 navigationController:(UINavigationController *)nc;

@end


