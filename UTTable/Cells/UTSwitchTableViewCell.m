//
//  UTSwitchTableViewCell.m
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTSwitchTableViewCell.h"

@interface UTSwitchTableViewCell()

@property (strong, nonatomic) UISwitch *switchView;

@end

@implementation UTSwitchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.switchView = [[UISwitch alloc] init];
        [self.switchView addTarget:self action:@selector(action:) forControlEvents:UIControlEventValueChanged];
        self.accessoryView = self.switchView;
    }
    return self;
}

- (void)action:(id)sender
{
    [self performAction];
}

- (BOOL)isOn
{
    return self.switchView.isOn;
}

- (void)setOn:(BOOL)on
{
    self.switchView.on = on;
}

@end

@implementation UTTableViewController(UTSwitchTableViewCell)

- (UTSwitchTableViewCell *)addSwitchCellWithText:(NSString *)text action:(UTCellActionBlock)action
{
    UTSwitchTableViewCell *cell = [[UTSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = text;
    cell.action = action;
    [self addCell:cell];
    return cell;
}

@end
