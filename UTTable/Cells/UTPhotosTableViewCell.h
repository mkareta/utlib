//
//  UTPhotosTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTableViewCell.h"

@protocol UTPhotosTableViewCellDelegate;

typedef void(^UTPhotosCompletionBlock)(UIImage *addedImage);

@interface UTPhotosTableViewCell : UTTableViewCell

@property (assign, nonatomic, readonly) NSInteger numberOfPhotos;
@property (assign, nonatomic, readonly) BOOL allowAddPhoto;

@property (weak, nonatomic) id <UTPhotosTableViewCellDelegate> delegate;

- (void)reloadPhotos;
- (void)insertPhotoAtIndex:(NSInteger)index;

@end

@protocol UTPhotosTableViewCellDelegate <NSObject>

@required
- (NSUInteger)numberOfPhotosForCell:(UTPhotosTableViewCell *)cell;
- (UIImage *)photoAtIndex:(NSInteger)index forCell:(UTPhotosTableViewCell *)cell;

@optional

// Default NO
- (BOOL)allowAddPhotosForCell:(UTPhotosTableViewCell *)cell;
- (void)configureAddPhotoView:(UIView *)view forPhotoCell:(UTPhotosTableViewCell *)cell;
- (void)photosCellAddPhoto:(UTPhotosTableViewCell *)cell;

@end

@interface UTTableViewController(UTPhotosTableViewCell)

- (UTPhotosTableViewCell *)addPhotosCellWithDelegate:(id<UTPhotosTableViewCellDelegate>)delegate;

@end

