//
//  UTSwitchTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTableViewCell.h"

@interface UTSwitchTableViewCell : UTTableViewCell

@property (assign, nonatomic, getter=isOn) BOOL on;
@property (strong, nonatomic, readonly) UISwitch *switchView;

@end

@interface UTTableViewController(UTSwitchTableViewCell)

- (UTSwitchTableViewCell *)addSwitchCellWithText:(NSString *)text action:(UTCellActionBlock)action;

@end
