//
//  UTOptionsPickerTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 18/03/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTPickerTableViewCell.h"

@class UTOptionsPickerTableViewCell;

@interface UTOptionsPickerTableViewCell : UTPickerTableViewCell

@property (strong, nonatomic, readonly) UIPickerView *pickerView;
@property (strong, nonatomic, readonly) NSArray<NSArray<NSString *> *> *options;
@property (strong, nonatomic, readonly) NSArray<NSArray<id> *> *values;
@property (strong, nonatomic, readonly) NSArray<id> *selectedValues;

@end

@interface UTTableViewController(UTNumberPickerTableViewCell)

- (UTOptionsPickerTableViewCell *)showOptionPickerForCell:(UTTableViewCell *)cell
                                                  options:(NSArray<NSArray<NSString *> *> *)options
                                                   values:(NSArray<NSArray<id<NSCopying>> *> *)values
                                           selectedValues:(NSArray<id<NSCopying>> *)values
                                                   action:(UTCellActionBlock)action;


@end
