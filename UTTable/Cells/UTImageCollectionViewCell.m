//
//  UTImageCollectionViewCell.m
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTImageCollectionViewCell.h"

@interface UTImageCollectionViewCell()

@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation UTImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView = [UIImageView new];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.contentView.clipsToBounds = YES;
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(0,
                                        0,
                                        CGRectGetWidth(self.contentView.frame),
                                        CGRectGetHeight(self.contentView.frame))];
}

- (void)prepareForReuse
{
    self.imageView.image = nil;
}

@end
