//
//  UTOptionsPickerTableViewCell.m
//  UTLib
//
//  Created by Max Kareta on 18/03/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTOptionsPickerTableViewCell.h"

@interface UTOptionsPickerTableViewCell() <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) NSArray<NSArray<NSString *> *> *options;
@property (strong, nonatomic) NSArray<NSArray<id> *> *values;
@property (strong, nonatomic) NSMutableArray<id> *mutableSelectedValues;


@end

@implementation UTOptionsPickerTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.pickerView = [[UIPickerView alloc] init];
        self.pickerView.translatesAutoresizingMaskIntoConstraints = NO;
        self.pickerView.dataSource = self;
        self.pickerView.delegate = self;
        [self.contentView addSubview:self.pickerView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.pickerView setFrame:CGRectMake(self.separatorInset.left,
                                         0,
                                         self.contentView.frame.size.width - self.separatorInset.left,
                                         self.contentView.frame.size.height)];

}

- (NSArray<id> *)selectedValues
{
    return [self.mutableSelectedValues copy];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.options.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.options[component].count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.options[component][row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.mutableSelectedValues[component] = self.values[component][row];
    [self performAction];
}

- (CGFloat)cellHeight
{
    [self.pickerView sizeToFit];
    return self.pickerView.frame.size.height;
}

@end

@implementation UTTableViewController(UTNumberPickerTableViewCell)

- (UTOptionsPickerTableViewCell *)showOptionPickerForCell:(UTTableViewCell *)cell
                                                  options:(NSArray<NSArray<NSString *> *> *)options
                                                   values:(NSArray<NSArray<id> *> *)values
                                           selectedValues:(NSArray<id<NSCopying>> *)selectedValues
                                                   action:(UTCellActionBlock)action
{
    NSAssert(options.count == values.count, @"validation error");
    [self.view endEditing:YES];
    if (self.currentPickerCell.sourceCell == cell) {
        [self hideCurrentPicker];
        return nil;
    }
    if (self.currentPickerCell) {
        [self hideCurrentPicker];
    }
    UTOptionsPickerTableViewCell *pickerCell = [[UTOptionsPickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    pickerCell.sourceCell = cell;
    pickerCell.options = options;
    pickerCell.values = values;
    pickerCell.action = action;
    pickerCell.mutableSelectedValues = [selectedValues mutableCopy];
    cell.highlighted = YES;
    
    [self insertCell:pickerCell afterCell:cell withAnimation:UITableViewRowAnimationFade];
    self.currentPickerCell = pickerCell;
    return pickerCell;
}

@end
