//
//  UTTextFieldTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 3/16/18.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTableViewCell.h"

@interface UTTextFieldTableViewCell : UTTableViewCell

@property (strong, nonatomic, readonly) UITextField *textField;

@end

@interface UTTableViewController(UTTextFieldTableViewCell)

- (UTTextFieldTableViewCell *)addTextFieldCellWithPlaceholder:(NSString *)placeholder text:(NSString *)text action:(UTCellActionBlock)action;
- (UTTextFieldTableViewCell *)addTextFieldCellWithPlaceholder:(NSString *)placeholder text:(NSString *)text userInfo:(id)userInfo action:(UTCellActionBlock)action;

@end
