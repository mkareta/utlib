//
//  UTPhotosTableViewCell.m
//  UTLib
//
//  Created by Max Kareta on 08/07/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTPhotosTableViewCell.h"
#import "UTImageCollectionViewCell.h"

static const CGFloat kPagerHeight = 44;
static NSString *kImageViewReuseIdentifier = @"imageView";
static NSString *kAddPhotoReuseIdentifier = @"addPhoto";

typedef NS_ENUM(NSInteger, UTViewTag)
{
    kUTViewTagUnknown = 0,
    kUTViewTagConfigured
};


@interface UTPhotosTableViewCell() <UICollectionViewDelegate, UICollectionViewDataSource>

@property (assign, nonatomic) BOOL isInited;
@property (assign, nonatomic) NSInteger numberOfPhotos;
@property (assign, nonatomic) BOOL allowAddPhoto;
@property (strong, nonatomic) UIPageControl *pageControl;

@property (strong, nonatomic) UICollectionViewFlowLayout *layout;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (assign, nonatomic) CGFloat overscroll;

@end

@implementation UTPhotosTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
    }
    return self;
}

- (CGFloat)cellHeight
{
    return 281.5;
}

- (void)willDisplay
{
    id<UTPhotosTableViewCellDelegate> delegate = self.delegate;
    NSAssert(delegate != nil, @"Please set delegate for UTPhotosTableViewCell");
    if (delegate && !self.isInited)
    {
        self.isInited = YES;
        self.allowAddPhoto = NO;
        if ([delegate respondsToSelector:@selector(allowAddPhotosForCell:)])
        {
            self.allowAddPhoto = [delegate allowAddPhotosForCell:self];
        }
        self.numberOfPhotos = [delegate numberOfPhotosForCell:self];
        self.layout = [[UICollectionViewFlowLayout alloc] init];
        [self.layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        self.layout.minimumLineSpacing = 0;
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
        [self.collectionView setShowsHorizontalScrollIndicator:NO];
        [self.collectionView setShowsVerticalScrollIndicator:NO];
        [self.collectionView setPagingEnabled:YES];
        [self.collectionView setBounces:NO];
        [self.collectionView setDirectionalLockEnabled:YES];
        [self.collectionView setAllowsSelection:NO];
        
        [self.collectionView registerClass:[UTImageCollectionViewCell class] forCellWithReuseIdentifier:kImageViewReuseIdentifier];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kAddPhotoReuseIdentifier];
        
        [self.contentView addSubview:self.collectionView];
        
        self.pageControl = [UIPageControl new];
        self.pageControl.hidesForSinglePage = YES;
        [self.contentView addSubview:self.pageControl];
        
        [self.collectionView reloadData];
        [self updatePageControl];
    }
}

- (void)reloadPhotos
{
    if (self.isInited)
    {
        [self.collectionView reloadData];
    }
}

- (void)setOverscrollValue:(CGFloat)overscroll
{
    self.overscroll = overscroll;
    [self setNeedsLayout];
}

- (void)updatePageControl
{
    self.pageControl.numberOfPages = self.numberOfPhotos + (self.allowAddPhoto ? 1 : 0);
}

- (void)insertPhotoAtIndex:(NSInteger)index
{
    NSAssert(index >= 0 && index <= self.numberOfPhotos, @"Invalid index");
    index = MIN(index, self.numberOfPhotos);
    index = MAX(0, index);
    self.numberOfPhotos++;
    [self updatePageControl];
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]];
}

- (void)didEndDisplay
{
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.pageControl setFrame:CGRectMake(0,
                                          CGRectGetHeight(self.contentView.frame) - kPagerHeight,
                                          CGRectGetWidth(self.contentView.frame),
                                          kPagerHeight)];
    [self.pageControl setNeedsDisplay];
    CGSize itemSize = CGSizeMake(CGRectGetWidth(self.contentView.frame), CGRectGetHeight(self.contentView.frame) - self.overscroll);
    CGRect collectionViewFrame = CGRectMake(0,
                                            self.overscroll,
                                            CGRectGetWidth(self.contentView.frame),
                                            CGRectGetHeight(self.contentView.frame) - self.overscroll);

    [self.layout invalidateLayout];
    [self.layout setItemSize:itemSize];
    [self.collectionView setContentSize:CGSizeMake(self.collectionView.contentSize.width,
                                                   CGRectGetHeight(self.contentView.frame) - self.overscroll)];
    [self.collectionView setFrame:collectionViewFrame];

}

#pragma mark - UICollectionViewSupport

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offset = scrollView.contentOffset.x;
    NSInteger currentPage = (NSInteger)round(offset / scrollView.frame.size.width);
    self.pageControl.currentPage = currentPage;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.numberOfPhotos + (self.allowAddPhoto ? 1 : 0);
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    typeof(self.delegate) delegate = self.delegate;
    NSAssert(delegate != nil, @"delegate can't be nil");
    if (indexPath.row < self.numberOfPhotos)
    {
        UTImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageViewReuseIdentifier forIndexPath:indexPath];
        cell.imageView.image = [delegate photoAtIndex:indexPath.row forCell:self];
        return cell;
    }
    else
    {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAddPhotoReuseIdentifier forIndexPath:indexPath];
        if (cell.tag == kUTViewTagUnknown)
        {
            cell.tag = kUTViewTagConfigured;
            NSAssert([delegate respondsToSelector:@selector(configureAddPhotoView:forPhotoCell:)],
                     @"allowAddPhotosForCell is YES, but configureAddPhotoView is not implemented");
            if ([delegate respondsToSelector:@selector(configureAddPhotoView:forPhotoCell:)])
            {
                [delegate configureAddPhotoView:cell.contentView forPhotoCell:self];
            }
        }
        return cell;
    }
}

@end

@implementation UTTableViewController(UTPhotosTableViewCell)

- (UTPhotosTableViewCell *)addPhotosCellWithDelegate:(id<UTPhotosTableViewCellDelegate>)delegate
{
    UTPhotosTableViewCell *cell = [[UTPhotosTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.delegate = delegate;
    [self addCell:cell];
    return cell;
}

@end

