//
//  UTPickerTableViewCell.h
//  UTLib
//
//  Created by Max Kareta on 18/03/2018.
//  Copyright © 2018 Max Kareta. All rights reserved.
//

#import "UTTableViewCell.h"

@interface UTPickerTableViewCell : UTTableViewCell

@property (strong, nonatomic) UTTableViewCell *sourceCell;

@end
