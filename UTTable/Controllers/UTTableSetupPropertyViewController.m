//
//  UTTableSetupPropertyViewController.m
//  UTLib
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableSetupPropertyViewController.h"

@interface UTTableSetupPropertyViewController ()

@property (strong, nonatomic) NSObject *object;
@property (strong, nonatomic) NSString *keyPath;

@end

@implementation UTTableSetupPropertyViewController

+ (NSArray *)optionsForValues:(NSArray *)values
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:values.count];
    for (NSObject *obj in values)
    {
        [result addObject:@{@"text" : [obj description], @"value" : obj}];
    }
    return result;
}

- (instancetype)initWithObject:(NSObject *)obj keyPath:(NSString *)keyPath values:(NSArray *)values
{
    self = [super initWithOptions:[UTTableSetupPropertyViewController optionsForValues:values]];
    if (nil != self)
    {
        self.object = obj;
        self.keyPath = keyPath;
    }
    return self;
}

- (void)didSelectOptions:(NSArray *)options
{
    [super didSelectOptions:options];
    NSObject *value = [options firstObject];
    [self.object setValue:value forKeyPath:self.keyPath];
}

@end
