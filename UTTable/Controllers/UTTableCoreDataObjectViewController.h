//
//  UTTableCoreDataObjectViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"

@import CoreData;

@interface UTTableCoreDataObjectViewController : UTTableViewController

@property (strong, nonatomic) NSManagedObject *object;

@end

@protocol UTCoreDataObjectActions <NSObject>

//TODO actions:

@end
