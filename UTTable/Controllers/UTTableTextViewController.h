//
//  UTTableTextViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTTableTextViewController : UIViewController

@property (strong, nonatomic) NSString *text;

- (void)setFontSize:(CGFloat)size;

@end
