//
//  UTTableArrayViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCollectionViewController.h"

@interface UTTableArrayViewController : UTTableCollectionViewController

/* Return name for object from array. Default implemetation return "Record #". 
 * Subclasses should overwrite this method if can exctract more information from record
 *
 * @param record Object from array
 * @param index Index of record
 */
- (NSString *)nameForRecord:(id)record atIndex:(NSUInteger)index;
- (void)setArray:(NSArray *)array;

@end

///Record may not conforms to UTTableRecord. They should at least contain UTTableRecordDescription method.
@protocol UTTableRecord <NSObject>

- (NSString *)UTTableRecordDescription;

@end

