//
//  UTTableDirectoryViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableDirectoryViewController.h"
#import "UTTableFileViewController.h"
#import "UTTableViewCell.h"

@interface UTTableDirectoryViewController ()

@end

@implementation UTTableDirectoryViewController

- (void)buildTable
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *files = [fileManager contentsOfDirectoryAtPath:self.path error:nil];
    UINavigationController *nc = self.navigationController;
    weakify(self);
    for (NSString *path in files)
    {
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", self.path, path];
        BOOL directory = NO;
        BOOL exists = [fileManager fileExistsAtPath:fullPath isDirectory:&directory];
        if ([self.extensions containsObject:path.pathExtension] && exists && !directory)
        {
            [self addPushCellWithText:path userInfo:fullPath actionBlock:^(UTTableViewCell *cell) {
                strongify(self);
                if (NULL != self.actionBlock)
                {
                    self.actionBlock(cell.userInfo);
                }
                else
                {
                    UTTableFileViewController *vc = [UTTableFileViewController new];
                    vc.filePath = cell.userInfo;
                    [nc pushViewController:vc animated:YES];
                }
            }];
        }
    }
}

@end
