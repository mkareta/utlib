//
//  UTTableArrayViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableArrayViewController.h"
#import "UTTableDictionaryViewController.h"

@interface UTTableArrayViewController ()

@property (strong, nonatomic) NSArray *dataArray;

@end

@implementation UTTableArrayViewController

- (void)buildTable
{
    [self.dataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *name = [self nameForRecord:obj atIndex:idx];
        [self addCellForObject:obj withName:name];
    }];
}

- (void)setArray:(NSArray *)array
{
    self.dataArray = array;
    [self rebuildItems];
}

- (NSString *)nameForRecord:(NSObject *)record atIndex:(NSUInteger)index
{
    NSString *name = nil;
    
    if ([record respondsToSelector:@selector(UTTableRecordDescription)])
    {
        name = [NSString stringWithFormat:@"%llu. %@", (u_int64_t)index, [(id <UTTableRecord>)record UTTableRecordDescription]];
    }
    else
    {
        name = [NSString stringWithFormat:@"Record %llu", (u_int64_t)index];
    }
    return name;
}

@end
