//
//  UTTableDictionaryViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCollectionViewController.h"

@interface UTTableDictionaryViewController : UTTableCollectionViewController

- (void)setDicionary:(NSDictionary *)dict;

@end
