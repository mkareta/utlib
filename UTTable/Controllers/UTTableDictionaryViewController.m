//
//  UTTableDictionaryViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableDictionaryViewController.h"

@interface UTTableDictionaryViewController ()

@property (strong, nonatomic) NSDictionary *dataDict;

@end

@implementation UTTableDictionaryViewController

- (void)buildTable
{
    [self.dataDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *name = [key isKindOfClass:[NSString class]] ? key : [key description];
        [self addCellForObject:obj withName:name];
    }];
}

- (void)setDicionary:(NSDictionary *)dict
{
    self.dataDict = dict;
    [self rebuildItems];
}


@end
