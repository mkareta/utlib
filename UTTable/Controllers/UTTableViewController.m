//
//  UTTableViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"
#import "UTTableViewCell.h"
#import "UTPickerTableViewCell.h"

@interface UTSection : NSObject

@property (strong, nonatomic) NSString *header;
@property (strong, nonatomic) NSString *footer;
@property (strong, nonatomic) NSMutableArray<UTTableViewCell *> *cells;

@end

@implementation UTSection

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.cells = [NSMutableArray new];
    }
    return self;
}

@end

@interface UTTableViewController ()

@property (strong, nonatomic) NSMutableArray<UTSection *> *sections;
@property (assign, nonatomic) BOOL isTableBuilt;
@property (assign, nonatomic) CGPoint savedContentOffset;

@end

@implementation UTTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.sections = [NSMutableArray new];
}

- (void)pushWithNavigationController:(UINavigationController *)nc
{
    [nc pushViewController:self animated:YES];
}

- (void)rebuildItems
{
    [self cleanTable];
    if (self.isViewLoaded && nil != self.view.window && !self.isTableBuilt)
    {
        [self.tableView beginUpdates];
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.sections.count)] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self buildTableInternal];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.sections.count)] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)reloadItems
{
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)buildTableInternal
{
    [self willBuildTable];
    [self startSectionWithHeader:nil];
    [self buildTable];
    [self didBuildTable];
}

- (UTTableViewCell *)addCell:(UTTableViewCell *)cell
{
    [self.sections.lastObject.cells addObject:cell];
    cell.section = self.sections.lastObject;
    return cell;
}

- (void)startSectionWithHeader:(NSString *)header
{
    [self startSectionWithHeader:header footer:nil];
}

- (void)startSectionWithHeader:(NSString *)header footer:(NSString *)footer
{
    if (self.sections.lastObject.cells.count == 0)
    {
        [self.sections removeLastObject];
    }
    UTSection *section = [UTSection new];
    section.header = header;
    section.footer = footer;
    [self.sections addObject:section];
}

- (void)willBuildTable
{
}

- (void)didBuildTable
{

}

- (void)buildTable
{
    
}

- (void)cleanOnViewDidDisappear
{
    [self cleanTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!self.isTableBuilt)
    {
        [super viewWillAppear:animated];
        [self buildTableInternal];
        [self.tableView reloadData];
        self.isTableBuilt = YES;
    }
    else
    {
        // Cells layout may be broken if we do not rebuild
        // table after viewDidDisappear
        [self.tableView setNeedsLayout];
    }
}


- (void)viewDidDisappear:(BOOL)animated
{
    [self cleanOnViewDidDisappear];
    [super viewDidDisappear:animated];
}

- (void)deleteCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation
{
    if (self.sections.count == 0)
    {
        return;
    }
    [cell.section.cells removeObject:cell];
    cell.section = nil;
    
    if (cell.indexPath)
    {
        [self.tableView deleteRowsAtIndexPaths:@[cell.indexPath] withRowAnimation:animation];
        cell.indexPath = nil;
    }
}

- (void)cleanTable
{
    [self.sections removeAllObjects];
    self.isTableBuilt = NO;
}

- (NSIndexPath *)indexPathForCell:(UTTableViewCell *)cell
{
    NSIndexPath *cellPath = cell.indexPath;
    if (!cellPath)
    {
        UTSection *section = cell.section;
        NSInteger sectionIndex = [self.sections indexOfObject:section];
        NSInteger row = [section.cells indexOfObject:cell];
        cellPath = [NSIndexPath indexPathForRow:row inSection:sectionIndex];
    }
    return cellPath;
}

- (void)insertCell:(UTTableViewCell *)newCell afterCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation
{
    if (self.sections.count == 0)
    {
        return;
    }
    NSIndexPath *cellPath = [self indexPathForCell:cell];
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:cellPath.row + 1 inSection:cellPath.section];
    [self.sections[newIndexPath.section].cells insertObject:newCell atIndex:newIndexPath.row];
    newCell.section = self.sections[newIndexPath.section];
    [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:animation];
}

- (void)insertCell:(UTTableViewCell *)newCell beforeCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation
{
    if (self.sections.count == 0)
    {
        return;
    }
    NSIndexPath *cellPath = [self indexPathForCell:cell];
    [self.sections[cellPath.section].cells insertObject:newCell atIndex:cellPath.row];
    newCell.section = self.sections[cellPath.section];
    [self.tableView insertRowsAtIndexPaths:@[cellPath] withRowAnimation:animation];
}

- (void)hideCurrentPicker
{
    if (!self.currentPickerCell)
    {
        return;
    }
    [self deleteCell:self.currentPickerCell withAnimation:UITableViewRowAnimationFade];
    self.currentPickerCell.sourceCell.highlighted = NO;
    self.currentPickerCell.sourceCell = nil;
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return [self.sections[section] footer];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sections[section] header];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sections[section].cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.sections[indexPath.section].cells[indexPath.row];
}

#pragma mark - Table view delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat overscroll = scrollView.contentOffset.y + scrollView.adjustedContentInset.top;
    CGFloat savedOverscroll = self.savedContentOffset.y + scrollView.adjustedContentInset.top;
    if (overscroll <= 0 || savedOverscroll < 0)
    {
        overscroll = MIN(overscroll, 0);
        [self.sections.firstObject.cells.firstObject setOverscrollValue:overscroll];
    }
    
    self.savedContentOffset = scrollView.contentOffset;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UTTableViewCell *cell = self.sections[indexPath.section].cells[indexPath.row];
    return cell.cellHeight;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [(UTTableViewCell *)cell setIndexPath:indexPath];
    [(UTTableViewCell *)cell willDisplay];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [(UTTableViewCell *)cell setIndexPath:nil];
    [(UTTableViewCell *)cell didEndDisplay];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UTTableViewCell *cell = self.sections[indexPath.section].cells[indexPath.row];
    return cell.canPerformAction ? indexPath : nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UTTableViewCell *cell = self.sections[indexPath.section].cells[indexPath.row];
    [cell performAction];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
