//
//  UTTableImageViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTTableImageViewController : UIViewController

@property (strong, nonatomic, readonly) NSObject *object;
@property (copy, nonatomic, readonly) NSString *keypath;

- (void)setObject:(NSObject *)object andKeyPath:(NSString *)keypath;

@end
