//
//  UTTableCollectionViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCollectionViewController.h"
#import "UTTableArrayViewController.h"
#import "UTTableDictionaryViewController.h"
#import "UTTableViewCell.h"
#import "UTTableCoreDataObjectViewController.h"

@import CoreData;

@interface UTTableCollectionViewController ()

@end

@implementation UTTableCollectionViewController

- (UTTableViewCell *)addCellForObject:(id)object withName:(NSString *)name
{
    return [self addCell:[self cellForObject:object withName:name]];
}

- (UTTableViewCell *)cellForObject:(id)object withName:(NSString *)name
{
    UINavigationController *nc = self.navigationController;
    if ([object isKindOfClass:[NSDictionary class]])
    {
        return [self addPushCellWithText:name actionBlock:^(UTTableViewCell *cell) {
            UTTableDictionaryViewController *vc = [UTTableDictionaryViewController new];
            [nc pushViewController:vc animated:YES];
            [vc setDicionary:cell.userInfo];
        }];
    }
    else if ([object isKindOfClass:[NSArray class]])
    {
        return [self addPushCellWithText:name actionBlock:^(UTTableViewCell *cell) {
            UTTableArrayViewController *vc = [UTTableArrayViewController new];
            [nc pushViewController:vc animated:YES];
            [vc setArray:cell.userInfo];
        }];
    }
    else if ([object isKindOfClass:[NSManagedObject class]])
    {
        return [self addPushCellWithText:name actionBlock:^(UTTableViewCell *cell) {
            UTTableCoreDataObjectViewController *vc = [UTTableCoreDataObjectViewController new];
            [vc setObject:cell.userInfo];
            [nc pushViewController:vc animated:YES];
        }];
    }
    else
    {
        return [self addTextViewerCellWithText:name text2:[object description] navigationController:self.navigationController];
    }
    return nil;
}

@end
