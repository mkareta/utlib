//
//  UTTableCoreDataObjectViewControlle.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCoreDataObjectViewController.h"
#import "UTTableArrayViewController.h"
#import "GCD+UTFunctions.h"
#import "UTTableViewCell.h"

@interface UTTableCoreDataObjectViewController ()

@property (strong, nonatomic) NSDictionary<NSString *, NSString *> *attributes;
@property (strong, nonatomic) NSDictionary<NSString *, id> *relationships;

@end

@implementation UTTableCoreDataObjectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadCoraDataTable:)];
    [self.navigationItem setRightBarButtonItem:item];
}

- (void)reloadCoraDataTable:(id)sender
{
    self.attributes = nil;
    self.relationships = nil;
    [self rebuildItems];
}

- (void)buildTable
{
    if (nil == self.object)
    {
        [self addCellWithText:@"Object is nil"];
    }
    else if (nil != self.attributes)
    {
        UINavigationController *nc = self.navigationController;
        [self startSectionWithHeader:@"Attributes"];
        NSArray *keys = [self.attributes.allKeys sortedArrayUsingSelector:@selector(compare:)];
        for (NSString *key in keys)
        {
            [self addTextViewerCellWithText:key text2:self.attributes[key] navigationController:self.navigationController];
        }
        [self startSectionWithHeader:@"Relationships"];
        NSArray *keys2 = [self.relationships.allKeys sortedArrayUsingSelector:@selector(compare:)];
        for (NSString *key in keys2)
        {
            id object = self.relationships[key];
            if (object == [NSNull null])
            {
                [self addCellWithText:key text2:@"nil"];
            }
            else if ([object isKindOfClass:[NSManagedObject class]])
            {
                NSString *text2 = NSStringFromClass([object class]);
                [self addPushCellWithText:key text2:text2 actionBlock:^(UTTableViewCell *cell) {
                    UTTableCoreDataObjectViewController *vc = [UTTableCoreDataObjectViewController new];
                    [vc setObject:object];
                    [nc pushViewController:vc animated:YES];
                }];
            }
            else
            {
                NSString *text2 = [NSString stringWithFormat:@"%lu objects", [object count]];
                if ([object count] == 0)
                {
                    [self addCellWithText:key text2:text2];
                }
                else
                {
                    [self addPushCellWithText:key text2:text2 actionBlock:^(UTTableViewCell *cell) {
                        UTTableArrayViewController *vc = [UTTableArrayViewController new];
                        [vc setArray:object];
                        [nc pushViewController:vc animated:YES];
                    }];
                }
            }
        }
    }
    else
    {
        [self addCellWithText:@"Loading..."];
        [self loadData];
    }
}

- (NSString *)stringDescriptionForAttribute:(NSAttributeDescription *)description object:(id)object
{
    if (!object)
    {
        return @"nil";
    }
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"ut_description_%@", description.name]);
    if ([object respondsToSelector:selector]) {
        IMP imp = [object methodForSelector:selector];
        return ((NSString *(*)(id, SEL))imp)(object, selector);
    }
    if (description.attributeType == NSBooleanAttributeType)
    {
        return [object boolValue] ? @"YES" : @"NO";
    }
    if ([object isKindOfClass:[NSData class]])
    {
        return [NSString stringWithFormat:@"NSData %tu bytes", [(NSData *)object length]];
    }
    if ([object isKindOfClass:[NSArray class]])
    {
        return [NSString stringWithFormat:@"Array %lu objects", [(NSArray *)object count]];
    }
    if ([object isKindOfClass:[NSDictionary class]])
    {
        return [NSString stringWithFormat:@"Dict %lu keys", [(NSDictionary *)object count]];
    }
    return [object description];
}

- (void)loadData
{
    [self.object.managedObjectContext performBlock:^{
        NSDictionary *attributes = [[self.object entity] attributesByName];
        NSDictionary *relationships = [[self.object entity] relationshipsByName];
        NSMutableDictionary *resultAttributes = [NSMutableDictionary dictionaryWithCapacity:attributes.count];
        NSMutableDictionary *resultRelationships = [NSMutableDictionary dictionaryWithCapacity:relationships.count];
        [attributes enumerateKeysAndObjectsUsingBlock:^(id key, NSAttributeDescription *attribute, BOOL *stop) {
            id object = [self.object valueForKey:key];
            resultAttributes[key] = [self stringDescriptionForAttribute:attribute object:object];
        }];
        [relationships enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([obj isToMany])
            {
                resultRelationships[key] = [[self.object valueForKey:key] allObjects];
            }
            else
            {
                NSManagedObject *value = [self.object valueForKey:key];
                resultRelationships[key] = value ?: [NSNull null];
            }
        }];
        dispatch_async_on_main_queue(^{
            self.relationships = resultRelationships;
            self.attributes = resultAttributes;
            [self rebuildItems];
        });
    }];
}

@end
