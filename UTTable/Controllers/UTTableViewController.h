//
//  UTTableViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

@class UTTableViewCell;
@class UTPickerTableViewCell;

@interface UTTableViewController : UITableViewController

@property (strong, nonatomic) UTPickerTableViewCell *currentPickerCell;

#pragma mark -
- (void)willBuildTable;
- (void)buildTable;
- (void)didBuildTable;
- (UTTableViewCell *)addCell:(UTTableViewCell *)cell;
- (void)startSectionWithHeader:(NSString *)title;
- (void)startSectionWithHeader:(NSString *)title footer:(NSString *)footer;

- (void)cleanOnViewDidDisappear;

- (void)pushWithNavigationController:(UINavigationController *)nc;
- (void)rebuildItems;

- (void)reloadItems;

- (void)hideCurrentPicker;

- (void)deleteCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation;
- (void)insertCell:(UTTableViewCell *)cell afterCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation;
- (void)insertCell:(UTTableViewCell *)cell beforeCell:(UTTableViewCell *)cell withAnimation:(UITableViewRowAnimation)animation;

@end


